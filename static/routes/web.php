<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
  return view('home');
});

Route::get('/join-now', function () {
  return view('judoontario.theme.defaultjudoontario::join-now');
});

Route::get('/olympians', function () {
  return view('olympians');
});

Route::get('/special-achievements', function () {
  return view('special-achievements');
});

Route::get('/JO-awards', function () {
  return view('JO-awards');
});

Route::get('/high-dans', function () {
  return view('high-dans');
});

Route::get('/hall-of-fame', function () {
  return view('hall-of-fame');
});
