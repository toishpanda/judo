import Slider from './Slider.vue'
import Slide from './Slide.vue'
import Effect from './Effect.vue'

function install (Vue, options) {
  Vue.component('citrus-slider', Slider)
  Vue.component('citrus-slide', Slide)
  Vue.component('citrus-effect', Effect)
}

export default { install }
