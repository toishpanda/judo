import AdRegion from './AdRegion.vue'

function install(Vue, options) {
  Vue.component('ad-region', AdRegion)
}

export default { install }
