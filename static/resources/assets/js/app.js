// Dependencies //
import Vue from 'vue'

// Components //
import MemberLink from './components/MemberLink.vue'
import Breadcrumbs from './components/Breadcrumbs.vue'
import NewsArticle from './components/NewsArticle.vue'
import EventCalendar from './components/EventCalendar.vue'
import NavItem from './components/NavItem.vue'
import ToggleNav from './components/ToggleNav.vue'

// Plugins //
import 'vue-event-calendar/dist/style.css' //^1.1.10, CSS has been extracted as one file, so you can easily update it.
import vueEventCalendar from 'vue-event-calendar'
import Slider from './components/Slider'
import AdRegion from './components/AdRegion'

Vue.use(Slider)
Vue.use(AdRegion)
Vue.use(vueEventCalendar, {locale: 'en'})

// Application Start //
let app;
window.addEventListener('load', () => {
  app = new Vue({
    el: '#app',
    components: {
      MemberLink,
      Breadcrumbs,
      NewsArticle,
      EventCalendar,
      NavItem,
      ToggleNav
    }
  })
})
