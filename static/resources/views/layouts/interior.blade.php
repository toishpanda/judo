@extends('layouts.generic.generic-bundled')

@section('head')

@endsection

@section('body')
  <div id="app">
    @include('partials.header')
    <div class="masthead" style="background-image: url(@yield('masthead-bg'))">
      <div class="masthead-content">
        @yield('masthead-content')
      </div>
    </div>
    <div class="topper">
      <div class="topper-content">
        <breadcrumbs></breadcrumbs>
      </div>
    </div>
    <main class="interior">
      @yield('content')
    </main>
    @include('partials.footer')
  </div>
@endsection
