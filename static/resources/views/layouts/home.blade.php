@extends('layouts.generic.generic-bundled')

@section('head')

@endsection

@section('body')
  <div id="app">
    @include('partials.header')
    <main>
      @yield('content')
    </main>
    @include('partials.footer')
  </div>
@endsection
