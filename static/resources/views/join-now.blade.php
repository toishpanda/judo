@extends('layouts.interior')

@section('masthead-bg', '/img/mastheads/join-now.jpg')
@section('masthead-content')
  <h2>
    Simple. Fast.<br/>
    Secure.<br/>
    Join now.
  </h2>
@endsection

@section('content')
  <div class="content">
    <h3>Let’s get started</h3>
    <div class="important">
      <div class="notice">
        <span>IMPORTANT</span><br />
        If you have forgotten your Username or Password, please use the <a href="/">Password Reset</a> utility and <strong>DO NOT</strong> create a second (new) account.
      </div>
      <div class="notice">
        <strong>For NEW Judo Ontario accounts</strong><br />
        Each Judoka must have their OWN unique account.  First time set-up uses a simple 3-step process.
      </div>
    </div>
    <div class="steps">
      <div class="step">
        <h4>Enter the Judoka information and ensure it’s correct</h4>
        <p>
          You’ll be filling out a form.  It is critical that the information is accurate and formatted correctly (i.e. spelling and capitalization).  In particular, ensure that your gender, birthdate, and club are correct.
        </p>
      </div>
      <div class="step">
        <h4>Enter your registration & payment information</h4>
        <p>
          You will be asked to submit your registration & payment information.  Please note: Your transaction will not be processed until after your Dojoshu (club owner/head instructor) reviews your information and approves your membership.
        </p>
      </div>
      <div class="step">
        <h4>Dojoshu approval</h4>
        <p>
          Your Dojoshu (club owner/head instructor) will review your information and approve your membership at which point your transaction will be processed.  Please contact your Dojoshu if there is a long delay in processing to ensure they s/he seen your application to join the club.
        </p>
      </div>
      <div class="judo-btn">Create a Judo Ontario member account now</div>
    </div>
  </div>
@endsection
