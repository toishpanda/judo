<header>
    <ad-region region="site-header" size="leaderboard"></ad-region>
    <div class="navigation-container">
        <div class="navigation-bar">
        <div class="menu-btn">
            <img src="/img/menu-btn.jpg">
        </div>
        <div class="logo">
            <a href="/">
            <img src="/img/logo.svg" alt="Judo Ontario Logo">
            </a>
        </div>
        <div class="controls">
            <div class="btn search"><img src="/img/icon-search.jpg" alt="Search Icon"/></div>
            <div class="btn login">
            <member-link to="login">
                LOG-IN
            </member-link>
            </div>
        </div>
        </div>
    </div>
</header>
