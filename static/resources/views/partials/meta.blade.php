<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta charset="UTF-8" />

<!-- Laravel Specific JS -->
@include('partials/laravel')

<!-- Typekit -->
<script src="https://use.typekit.net/zxo6gyc.js"></script>
<script>try { Typekit.load({ async: true }); } catch (e) { }</script>
