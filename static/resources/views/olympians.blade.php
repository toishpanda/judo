@extends('layouts.interior')

@section('masthead-bg', '/img/mastheads/join-now.jpg')
@section('masthead-content')
  <h2>
    The<br />
    pinnacle<br />
    of sport
  </h2>
@endsection

@section('content')
  <div class="content">
    <h3>Olympians</h3>
    <div class="hall-of-fame">
      <p>
        To become a successful Judoka with Judo Ontario is quite an achievement... but to become a successful Judoka at the Olympics, against competition from around the world requires a special honour.
      </p>
    </div>
    <div class="honors">
      <div class="row">
        <div class="year">
          <span>2004, 2008</span>
        </div>
        <div class="honor">
          <div class="image-box">
            <img src="img/olympians/bill.jpg" />
          </div>
        </div>
        <div class="honor">
          <h4>Bill Morgan</h4>
          <p>
            Bill has travelled around the world training and competing in World and Para-Olympic events. In addition, Bill has also competed in a number of able-bodied competitions. Bill has worked closely with his coach Tom Thompson, over several years and has excelled in international competitions. Bill's success in competing allowed him to qualify for the Olympics in Athens 2004 and  Beijing 2008.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="year">
          <span>2000</span>
        </div>
        <div class="honor">
          <div class="image-box">
            <img src="img/olympians/kim.jpg" />
          </div>
        </div>
        <div class="honor">
          <h4>Kimberly Orr (Ribble)</h4>
          <p>
            Kimberly was one of Ontario's world class athletes during the 1990's. She placed in a number of international competitions during her career and represented Canada with distinction. Kimberly is an NCCP certified level 3 coach and continues to coach at various national and international tournaments.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="year">
          <span>1996</span>
        </div>
        <div class="honor">
          <div class="image-box">
            <img src="img/olympians/natalie.jpg" />
          </div>
        </div>
        <div class="honor">
          <h4>Nathalie Gosselin</h4>
          <p>
            Nathalie is a former Canadian Champion and medalist competing in various international tournaments. Her career culminated in 1996 with a selection to the Canadian Olympic Team. Nathalie was a very passionate and skilled player who fought with a lot of heart. She currently heads up the ECLIPSE program for Judo Canada.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="year">
          <span>1992, 1996, 2000</span>
        </div>
        <div class="honor">
          <div class="image-box">
            <img src="img/olympians/michelle.jpg" />
          </div>
        </div>
        <div class="honor">
          <h4>Michelle Buckingham</h4>
          <p>
            Michelle was Ontario's only female three-time Olympian. She was a mainstay on the National Team and lead a very strong women's team in the 90's. A very powerful competitor who managed a number of top 8 finishes at the world level. Michelle placed in a number of international competitions and was several times the Canadian Champion.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="year">
          <span>1992</span>
        </div>
        <div class="honor">
          <div class="image-box">
            <img src="img/olympians/roman.jpg" />
          </div>
        </div>
        <div class="honor">
          <h4>Roman Hatashita</h4>
          <p>
            Roman was several times Canadian Champion and dominated his division for several years. Roman's career came to fruition with a selection to the 1992 Olympic Team. Currently, he owns and operates Hatashita Enterprises who has been a sponsor for Judo Ontario for some time now.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="year">
          <span>1988, 1992</span>
        </div>
        <div class="honor">
          <div class="image-box">
            <img src="img/olympians/placeholder.jpg" />
          </div>
        </div>
        <div class="honor">
          <h4>Sandra Greaves</h4>
          <p>
            Sandra was the first and only Canadian female athlete for Judo to qualify for the Olympics as a demonstration sport in 1988. Sandra was many times Canadian Champion and was a mainstay on the National Team for many years. She was a Pan American Games gold medalist as well as placing in a number of international tournaments. Sandra is a certified NCCP level 3 coach.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="year">
          <span>1988</span>
        </div>
        <div class="honor">
          <div class="image-box">
            <img src="img/olympians/craig.jpg" />
          </div>
        </div>
        <div class="honor">
          <h4>Craig Weldon</h4>
          <p>
            Craig is one of Canada's best athletes from the 1980's. He is a former Canadian Champion who was continually winning medals in international competition. Craig is a member of the “Who's Who” in Canadian Sport. Craig also made time to pursue his university education at Concordia University. He's a level 3 NCCP certified coach who holds the rank of 6th dan.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="year">
          <span>1984, 1998</span>
        </div>
        <div class="honor">
          <div class="image-box">
            <img src="img/olympians/glenn.jpg" />
          </div>
        </div>
        <div class="honor">
          <h4>Glenn Beauchamp</h4>
          <p>
            Glenn competed in two Olympic games and was very successful with a 5th place finish in Los Angeles in 1984. Glenn was many times Canadian Champion and did well on the international circuit. Glenn was a two-time World University medalist. Glenn is a very successful businessman who owns and operates a number of companies in Toronto.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="year">
          <span>1984, 1998</span>
        </div>
        <div class="honor">
          <div class="image-box">
            <img src="img/olympians/placeholder.jpg" />
          </div>
        </div>
        <div class="honor">
          <h4>Kevin Doherty</h4>
          <p>
            Kevin was a member of the 1984 and 1988 Olympic Teams. His greatest achievement came with a bronze medal at the 1981 World Championships. Kevin placed in a number of World Class tournaments and was several times Canadian Champion. He has held various positions on the Judo Ontario and Judo Canada Board of Directors. Kevin holds the rank of 7th dan.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="year">
          <span>1980, 1984, 1988</span>
        </div>
        <div class="honor">
          <div class="image-box">
            <img src="img/olympians/placeholder.jpg" />
          </div>
        </div>
        <div class="honor">
          <h4>Phil Takahashi</h4>
          <p>
            Phil was a member of the 1980-84-88 Olympic Teams. His greatest achievement came with a bronze medal at the 1981 World Championships. He was many times Canadian Champion and placed in a number of World class tournaments. Phil teaches a number of martial arts at the Takahashi Judo Club. Phil holds the rank of 7th dan.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="year">
          <span>1976</span>
        </div>
        <div class="honor">
          <div class="image-box">
            <img src="img/olympians/placeholder.jpg" />
          </div>
        </div>
        <div class="honor">
          <h4>Rainer Fischer</h4>
          <p>
            Rainer was a member of the 1976 Olympic Team. One of the highlights of his career was a gold medal in the 1975 Pan American Games. Rainer was many times Canadian Champion and was one of the first Ontario athletes to live an train at the National Training Centre in Montreal. Rainer opened the door for other Ontario athletes to move to the National Training Centre. Rainer holds the rank of 6th dan.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="year">
          <span>1976</span>
        </div>
        <div class="honor">
          <div class="image-box">
            <img src="img/olympians/wayne.jpg" />
          </div>
        </div>
        <div class="honor">
          <h4>Wayne Erdman</h4>
          <p>
            Wayne was a member of the 1976 Olympic Team. He is a certified Level 3 NCCP coach and a former National Coach. Wayne was a gold medalist at the 1975 Pan-American Games as well as being a Canadian Champion many times. He currently operates the Olympia Judo Club in Kitchener, ON. Wayne has also been a member of the Provincial Grading Board for many years. Wayne holds the rank of 7th dan.
          </p>
        </div>
      </div>
    </div>
  </div>
@endsection
