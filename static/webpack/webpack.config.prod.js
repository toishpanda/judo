const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CSSSplitWebpackPlugin = require('css-split-webpack-plugin').default;

const extractSass = new ExtractTextPlugin({
  filename: '[name].bundle.css'
});

const extractVueSass = new ExtractTextPlugin({
  filename: '[name].vue.bundle.css'
});

const sassLoader = [{
    loader: "css-loader"
  }, {
    loader: "postcss-loader", options: {
      config: {
        path: './webpack/postcss.config.js'
      }
    }
  }, {
    loader: "sass-loader"
}]

module.exports = {
  name: 'js',
  entry: {
    main: [ './resources/assets/js/app.js', './resources/assets/sass/core.scss' ]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [ 'env' ]
          }
        }
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            scss: extractVueSass.extract({
              use: sassLoader,
              fallback: 'vue-style-loader'
            })
          },
          postcss: [ require('autoprefixer')(), require('cssnano')() ]
        }
      },
      {
        test: /\.scss$/,
        use: extractSass.extract({
          allChunks: true,
          fallback: 'style-loader',
          use: sassLoader
        })
      },
      {
        test: /\.css$/,
        use: extractSass.extract({
          fallback: "style-loader",
          use: "css-loader?url=false"
        })
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {}
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'main',
      filename: '_commons.bundle.js'
    }),
    extractSass,
    extractVueSass,
    new CSSSplitWebpackPlugin({ size: 3000 }),
    new HtmlWebpackPlugin ({
      inject: false,
      hash: true,
      filename: '../../resources/views/layouts/generic/generic-bundled.blade.php',
      template: './resources/views/layouts/generic/generic.ejs'
    })
  ],
  output: {
    filename: '[name].bundle.js',
    publicPath: 'bundles/',
    path: path.join(__dirname, '../public/bundles')
  }
};
