const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CSSSplitWebpackPlugin = require('css-split-webpack-plugin').default;

const extractSass = new ExtractTextPlugin({
  filename: '[name].bundle.css'
});

const sassLoader = [{
    loader: "css-loader"
  }, {
    loader: "sass-loader"
}]

module.exports = {
  name: 'js',
  entry: {
    main: [ './resources/assets/js/app.js', './resources/assets/sass/core.scss' ]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [ 'env' ]
          }
        }
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            scss: extractSass.extract({
              use: sassLoader,
              fallback: 'vue-style-loader'
            })
          }
        }
      },
      {
        test: /\.scss$/,
        use: extractSass.extract({
          allChunks: true,
          fallback: 'style-loader',
          use: sassLoader
        })
      },
      {
        test: /\.css$/,
        use: extractSass.extract({
          fallback: "style-loader",
          use: "css-loader?url=false"
        })
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  target: 'web',
  plugins: [
    extractSass,
    new HtmlWebpackPlugin ({
      inject: false,
      hash: true,
      filename: '../../addons/judoontario/judoontario/defaultjudoontario-theme/resources/views/layouts/generic-bundled.twig',
      template: './resources/views/layouts/generic/generic.ejs'
    })
  ],
  output: {
    filename: '[name].bundle.js',
    publicPath: 'bundles/',
    path: path.join(__dirname, '../public/bundles')
  }
};
