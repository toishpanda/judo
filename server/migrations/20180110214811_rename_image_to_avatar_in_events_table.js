exports.up = function (knex, Promise) {
  return knex.schema.table('events', (t) => {
    t.renameColumn('image', 'avatar')
  }).then(() => {
    return knex.schema.alterTable('events', (t) => {
      t.string('avatar').nullable().alter()
    })
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('events', (t) => {
    t.renameColumn('avatar', 'image')
  }).then(() => {
    return knex.schema.alterTable('events', (t) => {
      t.string('image').notNullable().alter()
    })
  })
}
