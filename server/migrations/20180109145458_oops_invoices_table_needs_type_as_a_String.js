exports.up = function (knex, Promise) {
  return knex.schema.alterTable('invoices', (t) => {
    t.string('to_type').notNullable().alter()
    t.string('from_type').notNullable().alter()
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.alterTable('invoices', (t) => {
    t.integer('to_type').notNullable().alter()
    t.integer('from_type').notNullable().alter()
  })
}
