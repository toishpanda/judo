exports.up = function (knex, Promise) {
  return knex.schema.table('events', (t) => {
    t.integer('technical_director').unsigned().references('id').inTable('users')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('events', (t) => {
    t.dropColumn('technical_director')
  })
}
