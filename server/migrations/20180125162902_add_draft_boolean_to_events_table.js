exports.up = function (knex, Promise) {
  return knex.schema.table('events', (t) => {
    t.boolean('draft')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('events', (t) => {
    t.dropColumn('draft')
  })
}
