exports.up = function(knex, Promise) {
  return knex.schema.createTable('transactions', (t) => {
    t.increments('id').unsigned().primary()
    t.timestamps(true)

    t.integer('amount').notNull()
    t.string('description').notNull()
    t.json('data').notNull()
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('transactions')
};
