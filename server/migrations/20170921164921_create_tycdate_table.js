exports.up = function(knex, Promise) {
  return knex.schema.createTable('tycdates', (t) => {
    t.increments('id').unsigned().primary()
    t.timestamps(true)

    t.string('name').notNull()
    t.integer('age_lowest').notNull().unsigned()
    t.integer('age_highest').notNull().unsigned()
    t.integer('rank_lowest').notNull().unsigned()
    t.integer('rank_highest').notNull().unsigned()
    t.integer('price').notNull().unsigned()
    t.string('gender').notNull()
    t.string('type').notNull()
    t.string('address')
    t.string('city')
    t.string('province')
    t.string('country')
    t.string('postal')
    t.string('phone')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('tycdates')
};
