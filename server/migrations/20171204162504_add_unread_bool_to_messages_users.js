exports.up = function (knex, Promise) {
  return knex.schema.table('messages_users', (t) => {
    t.boolean('unread')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('messages_users', (t) => {
    t.dropColumn('unread')
  })
}
