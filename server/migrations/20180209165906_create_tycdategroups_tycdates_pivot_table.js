exports.up = function (knex, Promise) {
  return knex.schema.createTable('tycdategroups_tycdates', (t) => {
    t.integer('tycdategroup_id').notNull().unsigned().references('id').inTable('tycdategroups')
    t.integer('tycdate_id').notNull().unsigned().references('id').inTable('tycdates')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTable('tycdategroups_tycdates')
}
