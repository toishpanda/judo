exports.up = function(knex, Promise) {
  return knex.schema.createTable('clubs_users', (t) => {
    t.integer('user_id').notNull().unsigned().references('id').inTable('users')
    t.integer('club_id').notNull().unsigned().references('id').inTable('clubs')
  })
}

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('clubs_users')
}
