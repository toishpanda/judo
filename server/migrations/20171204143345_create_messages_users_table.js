exports.up = function (knex, Promise) {
  return knex.schema.createTable('messages_users', (t) => {
    t.increments('id').unsigned().primary()
    t.timestamps(true)

    t.integer('message_id').notNull().unsigned().references('id').inTable('messages')
    t.integer('user_id').notNull().unsigned().references('id').inTable('users')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTable('messages_users')
}
