exports.up = function (knex, Promise) {
  return knex.schema.createTable('invoices', (t) => {
    t.increments('id').unsigned().primary()
    t.timestamps(true)

    // From entity
    t.integer('from_id').notNull()
    t.integer('from_type').notNull()

    // To entity
    t.integer('to_id').notNull()
    t.integer('to_type').notNull()

    // Invoice data
    t.float('value', 2).notNull()
    t.json('items').notNull()
  })
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable('invoices')
};
