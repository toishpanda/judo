exports.up = function(knex, Promise) {
  return knex.schema.createTable('events', (t) => {
    t.increments('id').unsigned().primary()
    t.timestamps(true)

    t.string('name').notNull()
    t.string('image').notNull()
    t.json('tags').notNull()
    t.integer('discount_rule').notNull().unsigned()
    t.float('discount_effect').notNull()
    t.dateTime('register_start').notNull()
    t.dateTime('register_end').notNull()
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('events')
};
