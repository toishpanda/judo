exports.up = function(knex, Promise) {
  return knex.schema.createTable('events_tycdates', (t) => {
    t.integer('event_id').notNull().unsigned().references('id').inTable('events')
    t.integer('tycdate_id').notNull().unsigned().references('id').inTable('tycdates')
  })
}

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('events_tycdates')
}
