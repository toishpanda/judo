exports.up = function (knex, Promise) {
  return knex.schema.alterTable('messages', (t) => {
    t.string('content', 1024).notNull().alter()
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.alterTable('messages', (t) => {
    t.string('content', 255).notNull().alter()
  })
}
