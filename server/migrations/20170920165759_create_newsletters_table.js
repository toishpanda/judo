exports.up = function(knex, Promise) {
  return knex.schema.createTable('newsletters', (t) => {
    t.increments('id').unsigned().primary()
    t.timestamps(true)

    t.string('name').notNull()
    t.string('pdf').notNull()
    t.integer('club_id').notNull().unsigned().references('id').inTable('clubs')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('newsletters')
};
