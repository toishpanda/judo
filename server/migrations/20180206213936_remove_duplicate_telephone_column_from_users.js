exports.up = function (knex, Promise) {
  return knex.schema.table('users', (t) => {
    t.dropColumn('tel')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('users', (t) => {
    t.string('tel')
  })
}
