exports.up = function(knex, Promise) {
  return knex.schema.createTable('clubs', (t) => {
    t.increments('id').unsigned().primary()
    t.timestamps(true)

    t.string('name').notNull()
    t.string('description').notNull()
    t.integer('dojoshu_id').notNull().unsigned().references('id').inTable('users')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('clubs')
};
