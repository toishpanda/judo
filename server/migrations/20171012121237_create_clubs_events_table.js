exports.up = function(knex, Promise) {
  return knex.schema.createTable('clubs_events', (t) => {
    t.integer('event_id').notNull().unsigned().references('id').inTable('events')
    t.integer('club_id').notNull().unsigned().references('id').inTable('clubs')
  })
}

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('clubs_events')
}
