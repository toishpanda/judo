exports.up = function(knex, Promise) {
  return knex.schema.createTable('tycdategroups', (t) => {
    t.increments('id').unsigned().primary()
    t.timestamps(true)

    // For tycdates table
    t.integer('rank_lowest').notNull().unsigned()
    t.integer('rank_highest').notNull().unsigned()

    // For dates table
    t.dateTime('start').notNull()
    t.dateTime('end').notNull()
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('tycdategroups')
};
