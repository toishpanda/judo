exports.up = function (knex, Promise) {
  return knex.schema.table('tycdates', (t) => {
    t.integer('weight_lowest').unsigned()
    t.integer('weight_highest').unsigned()
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('tycdates', (t) => {
    t.dropColumn('weight_lowest')
    t.dropColumn('weight_highest')
  })
}
