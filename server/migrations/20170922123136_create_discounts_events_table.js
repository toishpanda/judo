exports.up = function(knex, Promise) {
  return knex.schema.createTable('discounts_events', (t) => {
    t.integer('discount_id').notNull().unsigned().references('id').inTable('discounts')
    t.integer('event_id').notNull().unsigned().references('id').inTable('events')
  })
}

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('discounts_events')
}
