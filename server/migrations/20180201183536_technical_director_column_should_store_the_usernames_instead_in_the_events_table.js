exports.up = function (knex, Promise) {
  return knex.schema.table('events', (t) => {
    t.dropForeign('technical_director')
  }).then(() => {
    return knex.schema.alterTable('events', (t) => {
      t.string('technical_director').alter()
    })
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.alterTable('events', (t) => {
    t.integer('technical_director').unsigned().references('id').inTable('users').alter()
  })
}
