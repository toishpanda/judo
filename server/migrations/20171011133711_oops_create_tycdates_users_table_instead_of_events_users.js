exports.up = function(knex, Promise) {
  return knex.schema.dropTable('events_users')
}

exports.down = function(knex, Promise) {
  return knex.schema.createTable('events_users', (t) => {
    t.integer('event_id').notNull().unsigned().references('id').inTable('events')
    t.integer('user_id').notNull().unsigned().references('id').inTable('users')
  })
}
