exports.up = function (knex, Promise) {
  return knex.schema.createTable('club_join_requests', (t) => {
    t.increments('id').unsigned().primary()
    t.timestamps(true)

    t.integer('user_id').notNull().unsigned().references('id').inTable('users')
    t.integer('club_id').notNull().unsigned().references('id').inTable('clubs')
  })
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable('club_join_requests')
};
