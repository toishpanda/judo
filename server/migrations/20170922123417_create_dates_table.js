exports.up = function(knex, Promise) {
  return knex.schema.createTable('dates', (t) => {
    t.increments('id').unsigned().primary()
    t.timestamps(true)

    t.dateTime('start').notNull()
    t.dateTime('end').notNull()
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('dates')
};
