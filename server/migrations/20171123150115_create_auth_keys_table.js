exports.up = function (knex, Promise) {
  return knex.schema.createTable('auth_keys', (t) => {
    t.increments('id').unsigned().primary()
    t.timestamps(true)

    t.string('type').notNull()
    t.string('key').notNull()
    t.json('data').notNull()
  })
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable('auth_keys')
};
