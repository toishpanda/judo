exports.up = function (knex, Promise) {
  return knex.schema.createTable('messages', (t) => {
    t.increments('id').unsigned().primary()
    t.timestamps(true)

    t.string('subject').notNull()
    t.string('content').notNull()
    t.integer('from').notNull().unsigned().references('id').inTable('users')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTable('messages')
}
