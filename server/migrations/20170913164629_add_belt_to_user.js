exports.up = function(knex, Promise) {
  return knex.schema.table('users', (t) => {
    t.string('belt')
  })
}

exports.down = function(knex, Promise) {
  return knex.schema.table('users', (t) => {
    t.dropColumn('belt')
  })
}
