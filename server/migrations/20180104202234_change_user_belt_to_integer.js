exports.up = function (knex, Promise) {
  return knex.schema.alterTable('users', (t) => {
    t.integer('belt').alter()
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.alterTable('users', (t) => {
    t.string('belt').alter()
  })
}
