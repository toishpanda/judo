exports.up = function(knex, Promise) {
  return knex.schema.createTable('dates_tycdates', (t) => {
    t.integer('date_id').notNull().unsigned().references('id').inTable('dates')
    t.integer('tycdate_id').notNull().unsigned().references('id').inTable('tycdates')
  })
}

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('dates_tycdates')
}
