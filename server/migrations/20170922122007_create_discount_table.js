exports.up = function(knex, Promise) {
  return knex.schema.createTable('discounts', (t) => {
    t.increments('id').unsigned().primary()
    t.timestamps(true)

    t.string('name').notNull()
    t.string('code').notNull()
    t.float('effect').notNull()
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('discounts')
};
