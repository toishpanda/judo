exports.up = function (knex, Promise) {
  return knex.schema.table('invoices', (t) => {
    t.dropColumn('value')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('invoices', (t) => {
    t.float('value', 2).notNull()
  })
}
