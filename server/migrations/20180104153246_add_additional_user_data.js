exports.up = function (knex, Promise) {
  return knex.schema.table('users', (t) => {
    t.string('firstname')
    t.string('lastname')
    t.string('tel')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('users', (t) => {
    t.dropColumn('firstname')
    t.dropColumn('lastname')
    t.dropColumn('tel')
  })
}
