exports.up = function (knex, Promise) {
  return knex.schema.table('lineitems', (t) => {
    t.boolean('is_registration_fee')
    t.boolean('is_club_fee')
    t.boolean('is_special_fee')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('lineitems', (t) => {
    t.dropColumn('is_registration_fee')
    t.dropColumn('is_club_fee')
    t.dropColumn('is_special_fee')
  })
}
