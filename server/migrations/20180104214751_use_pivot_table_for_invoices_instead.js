exports.up = function (knex, Promise) {
  return knex.schema.alterTable('invoices', (t) => {
    t.dropColumn('items')
  }).then(() => {
    return knex.schema.createTable('lineitems', (t) => {
      t.increments('id').unsigned().primary()
      t.string('name').notNull()
      t.string('description').notNull()
      t.float('value').notNull()
    })
  }).then(() => {
    return knex.schema.createTable('invoices_lineitems', (t) => {
      t.integer('invoice_id').notNull().unsigned().references('id').inTable('invoices')
      t.integer('lineitem_id').notNull().unsigned().references('id').inTable('lineitems')
    })
  })
};

exports.down = function (knex, Promise) {
  return knex.schema.table('invoices', (t) => {
    t.json('items').notNull()
  }).then(() => {
    return knex.schema.dropTable('invoices_lineitems')
  }).then(() => {
    return knex.schema.dropTable('lineitems')
  })
};
