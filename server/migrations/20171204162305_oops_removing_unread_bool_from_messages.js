exports.up = function (knex, Promise) {
  return knex.schema.table('messages', (t) => {
    t.dropColumn('unread')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('messages', (t) => {
    t.boolean('unread')
  })
}
