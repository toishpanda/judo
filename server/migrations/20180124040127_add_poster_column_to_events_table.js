exports.up = function (knex, Promise) {
  return knex.schema.table('events', (t) => {
    t.string('poster')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('events', (t) => {
    t.dropColumn('poster')
  })
}
