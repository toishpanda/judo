exports.up = function(knex, Promise) {
  return knex.schema.table('users', (t) => {
    // Member ID
    t.integer('judo_ontario_number')
    t.integer('judo_canada_number')
    t.integer('nccp_number')

    // Location Data
    t.string('address')
    t.string('city')
    t.string('province')
    t.string('country')
    t.string('postal')
    t.string('phone')

    // Member State
    t.boolean('aboriginal')
    t.boolean('disabled')
    t.boolean('active')
    t.boolean('admin')
    t.integer('nccp_level')

    // Other Member Data
    t.string('parent_name')
    t.string('avatar')
  })
}

exports.down = function(knex, Promise) {
  return knex.schema.table('users', (t) => {
    // Member ID
    t.dropColumn('judo_ontario_number')
    t.dropColumn('judo_canada_number')
    t.dropColumn('nccp_number')

    // Location Data
    t.dropColumn('address')
    t.dropColumn('city')
    t.dropColumn('province')
    t.dropColumn('country')
    t.dropColumn('postal')
    t.dropColumn('phone')

    // Member State
    t.dropColumn('aboriginal')
    t.dropColumn('disabled')
    t.dropColumn('active')
    t.dropColumn('admin')
    t.dropColumn('nccp_level')

    // Other Member Data
    t.dropColumn('parent_name')
    t.dropColumn('avatar')
  })
}
