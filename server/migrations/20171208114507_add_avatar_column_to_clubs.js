exports.up = function (knex, Promise) {
  return knex.schema.table('clubs', (t) => {
    t.string('avatar')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('clubs', (t) => {
    t.dropColumn('avatar')
  })
}
