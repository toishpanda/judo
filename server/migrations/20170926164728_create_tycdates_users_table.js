exports.up = function(knex, Promise) {
  return knex.schema.createTable('tycdates_users', (t) => {
    t.integer('user_id').notNull().unsigned().references('id').inTable('users')
    t.integer('tycdate_id').notNull().unsigned().references('id').inTable('tycdates')
  })
}

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('tycdates_users')
}
