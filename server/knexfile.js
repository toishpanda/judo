require('dotenv').config()

// Use the .env file instead of editing this. //

module.exports = {

  development: {
    client: 'pg',
    connection: {
      host: process.env.POSTGRES_HOST || 'postgres',
      port: process.env.POSTGRES_PORT,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB
    }
  },

  staging: {
    client: 'pg',
    connection: {
      host: process.env.POSTGRES_HOST || 'postgres',
      port: process.env.POSTGRES_PORT,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB
    }
  },

  production: {
    client: 'pg',
    connection: {
      host: process.env.POSTGRES_HOST || 'postgres',
      port: process.env.POSTGRES_PORT,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB
    }
  }

}
