let path = require('path')
let webpack = require('webpack')
var nodeExternals = require('webpack-node-externals')

module.exports = {
  entry: './src/entry',

  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'index.js',
    libraryTarget: 'commonjs2'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env'],
            plugins: ['transform-runtime']
          }
        }
      }
    ]
  },

  externals: [nodeExternals()],

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        // These values replace ~/.env values.
        'KOA_PORT': 8080
      }
    })
  ],

  resolve: {
    modules: [
      'node_modules',
      path.resolve(__dirname, 'app')
    ],
    extensions: ['.js']
  },

  node: {
    __dirname: true
  },

  target: 'node'
}
