var path = require('path')
var nodeExternals = require('webpack-node-externals')

module.exports = {
  entry: './src/entry',

  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'index.js',
    libraryTarget: 'commonjs2'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env'],
              plugins: ['transform-runtime']
            }
          },
          {
            loader: 'eslint-loader'
          }
        ]
      }
    ]
  },

  externals: [nodeExternals()],

  resolve: {
    modules: [
      'node_modules',
      path.resolve(__dirname, 'app')
    ],
    extensions: ['.js']
  },

  node: {
    __dirname: true
  },

  target: 'node'
}
