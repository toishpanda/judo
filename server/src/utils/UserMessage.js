import db from '../db.js'
import generateTables from '../tables'

const tables = generateTables(db)

/** UserMessage class, used for sending messages to user's judo ontario inboxes. */
class UserMessage {
  /**
   * @param {string|[string]} to - The username[s] (or ID[s]) to send the message to
   * @param {string} subject - Subject line of the message
   * @param {string} content - Message content
   */
  constructor (to, subject, content) {
    this.to = typeof to === 'string' || typeof to === 'number' ? [to] : to
    this.subject = subject
    this.content = content
  }

  /**
   * Send message to inbox(es)
   * @param {Number} fromUserID - The ID of the user to send the message from
   */
  async send (fromUserID) {
    if (fromUserID === undefined) { console.error('UserMessage: send() must include a \'from\' user ID'); return }
    const messageID = await tables.Message.insert({
      subject: this.subject,
      content: this.content,
      from: fromUserID
    }, ['id']).then((res) => res.id)

    const users = await Promise.all(this.to.map((username) => {
      if (typeof username === 'string') return tables.User.findBy('username', username)
      if (typeof username === 'number') return tables.User.find(username)
      else return new Promise((resolve) => resolve({id: username}))
    }))

    const validUsers = users.every((result) => Object.keys(result).length > 0)

    if (validUsers) {
      return tables.Message.attachUsers(messageID, users.map((user) => user.id))
    } else {
      console.error('UserMessage: send() must contain either usernames or IDs in the \'to\' field.')
      return new Promise((resolve) => resolve(false))
    }
  }
}

export { UserMessage }
