import db from '../db.js'

/** Auth Token class, used for password resets, and any other time we need to authenticate a user without using a password, 2FA style. */
class Token {
  /**
   * @param {string} type - Type of token, e.g. 'password-reset'
   * @param {object} data - Data stored with token
   * @param {string} [key] - The key the user will need to enter. If left out, it will be automatically generated
   */
  constructor (type, data, key) {
    this.type = type
    this.data = data
    this.key = key || Math.floor((1 + Math.random()) * 0x10000).toString(16)
  }

  /**
   * Update or insert the token in the database
   */
  async register () {
    const keyExists = await db('auth_keys').select(1).where('key', '=', this.key).first().then((key) => !!key)
    if (keyExists) {
      await db('auth_keys').where('key', this.key).update({
        type: this.type,
        data: this.data,
        key: this.key,
        updated_at: db.fn.now()
      })
    } else {
      await db('auth_keys').insert({
        type: this.type,
        data: this.data,
        key: this.key,
        updated_at: db.fn.now(),
        created_at: db.fn.now()
      })
    }
  }

  async unregister () {
    await db('auth_keys').where('key', this.key).del()
  }

  /**
   * Make a link to the password reset page with the key pre-loaded, for use in emails.
   * @param {string} linkText - The text to display inside the link
   */
  makeLink (linkText) {
    return `<a href="http://localhost:9090/api/auth/${this.type}/${this.key}">${linkText}</a>`
  }
}

async function getToken (key) {
  const token = await db('auth_keys').select('*').where('key', key).first()
  if (token) {
    return new Token(token.type, token.data, token.key)
  } else {
    return false
  }
}

export { Token, getToken }
