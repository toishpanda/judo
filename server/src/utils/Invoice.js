import db from '../db.js'
import generateTables from '../tables'

import axios from 'axios'

const tables = generateTables(db)

/** Invoice class API */
class Invoice {
  constructor () {
    this.commited = false
    this.invoice = null
    this.moneris = {
      hpp_id: null,
      order_id: null,
      ticket: null
    }
    this.from = { id: null, table: null }
    this.to = { id: null, table: null }
    this.lineitems = []
  }

  toUser (userID) {
    this.to = { id: userID, table: 'users' }
    this.commited = false
    return this
  }

  toClub (clubID) {
    this.to = { id: clubID, table: 'clubs' }
    this.commited = false
    return this
  }

  fromUser (userID) {
    this.from = { id: userID, table: 'users' }
    this.commited = false
    return this
  }

  fromClub (clubID) {
    this.from = { id: clubID, table: 'clubs' }
    this.commited = false
    return this
  }

  appendItem (type, name, description, value) {
    this.lineitems.push({
      type,
      name,
      description,
      value
    })
    return this
  }

  preload () {
    if (this.commited) {
      return axios.post('https://esqa.moneris.com/HPPDP/index.php', {
        ps_store_id: '', // For moneris account
        hpp_key: '', // For moneris account
        hpp_preload: null, // intentionally null
        charge_total: this.lineitems.reduce((sum, lineitem) => sum + lineitem.value, 0)
      }).then((res) => {
        this.moneris = {
          hpp_id: res.body.hpp_id,
          order_id: res.body.order_id,
          ticket: res.body.ticket
        }
        return this.moneris
      }).catch(() => {
        this.moneris = {}
        return false
      })
    } else {
      console.warn('INVOICE: Invoices must be commited to the DB before preloaded')
      return false
    }
  }

  async commit () {
    if (this.invoice === null) {
      const lineitemIDs = await Promise.all(this.lineitems.map((lineitem) => {
        return tables.Invoice.newLineItem(
          lineitem.type,
          lineitem.name,
          lineitem.description,
          lineitem.value
        )
      }))

      const invoiceID = await tables.Invoice.insert({
        to_id: this.to.id,
        to_type: this.to.table,
        from_id: this.from.id,
        from_type: this.from.table
      }, ['id']).then((invoice) => invoice.id)

      await tables.Invoice.attachItems(invoiceID, lineitemIDs)
    } else {
      // @TODO update invoice that already exists
      console.log('updating an invoice that already exists isn\'t supprted yet')
    }

    this.commited = true
  }
}

export default Invoice
