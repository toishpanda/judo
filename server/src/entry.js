import configDotEnv from 'dotenv'
import Controllers from './controllers'

import Koa from 'koa'
import KoaBody from 'koa-body'
import KoaCors from 'kcors'
import KoaJSON from 'koa-json'
import KoaSession from 'koa-session'

// Get configuration options from ~/.env and load them into `process.env`
configDotEnv.config()

// Init Koa application
const app = new Koa()

// Logger
if (process.env.KOA_VERBOSE) {
  app.use(async (ctx, next) => {
    const start = Date.now()
    await next()
    const ms = Date.now() - start
    console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)
  })
}

// Sessions
app.keys = [process.env.KEY]

app.use(KoaSession({}, app))

// Body Parser
app.use(KoaBody({ multipart: true }))

// JSON responses
app.use(KoaJSON({ pretty: false }))

// Allow for Cross Origin requests
app.use(KoaCors({ credentials: true }))

// -------------------------------------- //
// ALL MIDDLEWARE MUST BE ABOVE THIS LINE //
// -------------------------------------- //

// Setup router
const routes = Controllers()
app.use(routes)

// Lift off!
app.listen(process.env.KOA_PORT)

// A little feedback
console.log('Judo Ontario server started on port ' + process.env.KOA_PORT)
