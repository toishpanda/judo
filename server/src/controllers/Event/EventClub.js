/**
 * Get all clubs associated with event
 * GET /events/:id/clubs
 * @param {*} ctx
 * @param {*} next
 */
export async function getClubs (ctx, next) {
  const clubs = await this.tables.Event.getClubs(ctx.params.id, ['*'])

  ctx.body = {
    success: true,
    clubs
  }
}
