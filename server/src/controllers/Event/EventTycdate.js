import _without from 'lodash.without'

/**
 * Get all TYCDATES for event
 * GET /events/:eventID/tycdates
 * @param {*} ctx
 * @param {*} next
 */
export async function getTycdates (ctx, next) {
  const tycdates = await this.tables.Event.getTycdates(ctx.params.eventID, ['*'])

  ctx.body = {
    success: true,
    tycdates
  }
}

/**
 * Get TYCDATE info out of an event
 * GET /events/:eventID/tycdates/:tycdateID
 * @param {*} ctx
 * @param {*} next
 */
export async function getTycdate (ctx, next) {
  if (await this.tables.Tycdate.isPartOfEvent(ctx.params.tycdateID, ctx.params.eventID)) {
    const tycdate = await this.tables.Tycdate.find(ctx.params.tycdateID)

    ctx.body = {
      success: true,
      tycdate
    }
  } else {
    ctx.status = 404
    ctx.body = {
      success: false,
      message: `Event ID #${ctx.params.eventID} does not contain TYCDATE ID #${ctx.params.tycdateID}`
    }
  }
}

/**
 * Update a TYCDATE with new data
 * PUT /events/:eventID/tycdates/:tycdateID
 * @param {*} ctx
 * @param {*} next
 */
export async function putTycdate (ctx, next) {
  if (await this.tables.Tycdate.isPartOfEvent(ctx.params.tycdateID, ctx.params.eventID)) {
    if (ctx.request.body) {
      // Filter out any bad input
      let columns = _without(await this.tables.Tycdate.getColumns(), 'id')
      await this.tables.Tycdate.update(ctx.params.tycdateID, Object.keys(ctx.request.body).reduce((acc, key) => {
        return (columns.includes(key) ? Object.assign(acc, { [key]: ctx.request.body[key] }) : acc)
      }, {}))

      ctx.body = {
        success: true
      }
    } else {
      ctx.status = 400
      ctx.body = {
        success: false,
        message: `You must pass in tycdate data`
      }
    }
  } else {
    ctx.status = 404
    ctx.body = {
      success: false,
      message: `Event ID #${ctx.params.eventID} does not contain TYCDATE ID #${ctx.params.tycdateID}`
    }
  }
}

/**
 * Add a TYCDATE to an event
 * POST /events/:eventID/tycdates
 * @param {*} ctx
 * @param {*} next
 */
export async function postTycdate (ctx, next) {
  const tycdateID = await (this.tables.Tycdate.insert({
    name: ctx.request.body.name,
    age_lowest: ctx.request.body.age_lowest,
    age_highest: ctx.request.body.age_highest,
    rank_lowest: ctx.request.body.rank_lowest,
    rank_highest: ctx.request.body.rank_highest,
    price: ctx.request.body.price,
    gender: ctx.request.body.gender,
    type: ctx.request.body.type,
    address: ctx.request.body.address,
    city: ctx.request.body.city,
    province: ctx.request.body.province,
    country: ctx.request.body.country,
    postal: ctx.request.body.postal,
    phone: ctx.request.body.phone
  }, ['id']).then((res) => res.id))

  await this.tables.Event.attachTycdate(ctx.params.eventID, tycdateID)

  ctx.body = {
    success: true,
    id: tycdateID
  }
}

/**
 * Remove a TYCDATE from an event + delete
 * DELETE /events/:eventID/tycdates/:tycdateID
 * @param {*} ctx
 * @param {*} next
 */
export async function deleteTycdate (ctx, next) {
  await this.tables.Tycdate.atomicDelete(ctx.params.tycdateID)

  ctx.body = {
    success: true
  }
}
