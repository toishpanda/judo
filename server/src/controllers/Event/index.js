import SecureRouter from '../parents/SecureRouter'

import * as EventClub from './EventClub'
import * as EventTycdate from './EventTycdate'
import * as EventTycdateUser from './EventTycdateUser'

// This is the JSON representation of the basic event template's TYCDATES
import basicEventTycdates from './basicEventTycdates.json'

import { getImageFromRequest } from '../utils/imageUpload'

class Event extends SecureRouter {
  constructor () {
    super('/events')

    // Self
    this._get('/', this.getEvents) // Get all
    this._get('/:id', this.getEvent) // Get one
    this._post('/', this.postEvent) // Create
    this._put('/:id', this.putEvent) // Update
    this._put('/:id/avatar', this.putAvatar) // Update Avatar (MULTIPART)
    this._put('/:id/poster', this.putPoster) // Update Poster (MULTIPART)
    this._delete('/:id', this.deleteEvent) // Delete
    this._post('/copy-of/:id', this.copyEvent) // Copy one
    this._put('/populate/:id', this.putPopulateEvent) // populate one event with generic data

    // Event->Club
    this._get('/:id/clubs', EventClub.getClubs) // Get all

    // Event->TYCDATE
    this._get('/:eventID/tycdates', EventTycdate.getTycdates) // Get all
    this._get('/:eventID/tycdates/:tycdateID', EventTycdate.getTycdate) // Get one
    this._post('/:eventID/tycdates', EventTycdate.postTycdate) // Create
    this._put('/:eventID/tycdates/:tycdateID', EventTycdate.putTycdate) // Edit
    this._delete('/:eventID/tycdates/:tycdateID', EventTycdate.deleteTycdate) // Edit

    // Event->TYCDATE->User
    this._get('/:eventID/tycdates/:tycdateID/users', EventTycdateUser.getUsers) // Get all
    this._post('/:eventID/tycdates/:tycdateID/users', EventTycdateUser.registerUserForTycdate) // Create
  }

  /**
   * Get all events
   * GET /events
   * @param {*} ctx
   * @param {*} next
   */
  async getEvents (ctx, next) {
    let events = await this.tables.Event.select('*').where('draft', '=', false).orderBy('id', 'desc')
    const tycdates = await Promise.all(events.map((event) => {
      return this.tables.Event.getTycdates(event.id)
    }))
    const clubs = await Promise.all(events.map((event) => {
      return this.tables.Event.getClubs(event.id, ['name', 'id', 'avatar'])
    }))

    events = events.map((event, index) => {
      return {
        event,
        clubs: clubs[index],
        tycdates: tycdates[index]
      }
    })

    ctx.body = {
      success: true,
      events
    }
  }

  /**
   * Get a single event
   * GET /events/:id
   * @param {*} ctx
   * @param {*} next
   */
  async getEvent (ctx, next) {
    const event = await this.tables.Event.find(ctx.params.id)
    if (event) {
      const tycdates = await this.tables.Event.getTycdates(ctx.params.id)
      const clubs = await this.tables.Event.getClubs(event.id, ['name', 'id', 'avatar'])

      ctx.body = {
        success: true,
        event,
        clubs,
        tycdates
      }
    } else {
      ctx.status = 404
      ctx.body = {
        success: false,
        message: `Event ID #${ctx.params.id} does not exist`
      }
    }
  }

  /**
   * Create a new event
   * POST /events
   * @param {*} ctx
   * @param {*} next
   */
  async postEvent (ctx, next) {
    const usersClub = await this.tables.Club.findBy('dojoshu_id', ctx.session.loggedInAs.id)
    if (Object.keys(usersClub).length) {
      const event = await this.tables.Event.insert({
        draft: (ctx.request.body.draft !== null ? ctx.request.body.draft : true),
        name: ctx.request.body.name,
        technical_director: ctx.request.body.technical_director,
        tags: JSON.stringify(ctx.request.body.tags),
        discount_rule: ctx.request.body.discount_rule,
        discount_effect: ctx.request.body.discount_effect,
        register_start: new Date(ctx.request.body.register_start),
        register_end: new Date(ctx.request.body.register_end)
      }, ['id', 'name'])

      await this.tables.Club.attachEvent(usersClub.id, event.id)

      ctx.body = {
        success: true,
        id: event.id
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'You are not affiliated with any club'
      }
    }
  }

  async populateEvent (eventID) {
    const tycdateTemplate = basicEventTycdates // Pulled in from JSON file, see imports at top of this file

    await Promise.all(Object.keys(tycdateTemplate).map((key) => {
      let genderSets = tycdateTemplate[key]

      return Promise.all(
        Object.keys(genderSets).map((gender) => {
          let weights = tycdateTemplate[key][gender]

          return Promise.all(
            weights.map(async (weight) => {
              let tycdate = await this.tables.Tycdate.insert({
                name: `${gender} ${key} ${weight.from}kg-${weight.to}kg`,
                age_lowest: 1,
                age_highest: 999,
                rank_lowest: 1,
                rank_highest: 5,
                weight_lowest: weight.from,
                weight_highest: weight.to,
                price: 0,
                gender,
                type: key,
                address: '',
                city: '',
                province: '',
                country: '',
                postal: '',
                phone: ''
              }, ['id'])

              await this.tables.Event.attachTycdate(eventID, tycdate.id)
            })
          )
        })
      )
    }))
  }

  /**
   * Put basic TYCDATE information into an event
   * PUT /events/populate/:id
   * @param {*} ctx
   * @param {*} next
   */
  async putPopulateEvent (ctx, next) {
    const eventExists = await this.tables.Event.exists(ctx.params.id)

    if (eventExists) {
      const usersClub = await this.tables.Club.findBy('dojoshu_id', ctx.session.loggedInAs.id)
      if (Object.keys(usersClub).length) {
        const eventsClubIDs = await this.tables.Event.getClubs(ctx.params.id, ['id']).then((ids) => ids.map((id) => id.id))
        if (eventsClubIDs.indexOf(usersClub.id) >= 0) {
          await this.populateEvent(ctx.params.id)

          ctx.body = {
            success: true
          }
        } else {
          ctx.status = 401
          ctx.body = {
            success: false,
            message: 'You are not allowed to edit this event'
          }
        }
      } else {
        ctx.status = 401
        ctx.body = {
          success: false,
          message: 'You are not affiliated with any club'
        }
      }
    } else {
      ctx.status = 404
      ctx.body = {
        success: false,
        message: `Club #${ctx.params.id} does not exist`
      }
    }
  }

  /**
   * Upload a new avatar for the event
   * PUT /events/:id/avatar
   * @param {*} ctx
   * @param {*} next
   */
  async putAvatar (ctx, next) {
    const usersClub = await this.tables.Club.findBy('dojoshu_id', ctx.session.loggedInAs.id)
    if (Object.keys(usersClub).length) {
      const eventsClubIDs = await this.tables.Event.getClubs(ctx.params.id, ['id']).then((ids) => ids.map((id) => id.id))
      if (eventsClubIDs.indexOf(usersClub.id) >= 0) {
        if (Object.keys(ctx.request.body.files).includes('avatar')) {
          const url = getImageFromRequest(ctx, 'avatars', 'event-avatar')
          if (url) {
            await this.tables.Event.updateWhere('id', ctx.params.id, {
              avatar: url
            })

            ctx.body = {
              success: true
            }
          }
        } else {
          ctx.status = 400
          ctx.body = {
            success: false,
            message: 'You need to supply a multipart request with a file named "avatar"'
          }
        }
      } else {
        ctx.status = 401
        ctx.body = {
          success: false,
          message: 'You are not allowed to edit this event'
        }
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'You are not affiliated with any club'
      }
    }
  }

  /**
 * Upload a new poster for the event
 * PUT /events/:id/poster
 * @param {*} ctx
 * @param {*} next
 */
  async putPoster (ctx, next) {
    const usersClub = await this.tables.Club.findBy('dojoshu_id', ctx.session.loggedInAs.id)
    if (Object.keys(usersClub).length) {
      const eventsClubIDs = await this.tables.Event.getClubs(ctx.params.id, ['id']).then((ids) => ids.map((id) => id.id))
      if (eventsClubIDs.indexOf(usersClub.id) >= 0) {
        if (Object.keys(ctx.request.body.files).includes('avatar')) {
          const url = getImageFromRequest(ctx, 'avatars', 'event-avatar')
          if (url) {
            await this.tables.Event.updateWhere('id', ctx.params.id, {
              poster: url
            })

            ctx.body = {
              success: true
            }
          }
        } else {
          ctx.status = 400
          ctx.body = {
            success: false,
            message: 'You need to supply a multipart request with a file named "poster"'
          }
        }
      } else {
        ctx.status = 401
        ctx.body = {
          success: false,
          message: 'You are not allowed to edit this event'
        }
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'You are not affiliated with any club'
      }
    }
  }

  /**
   * Update an existing event
   * PUT /events/:id
   * @param {*} ctx
   * @param {*} next
   */
  async putEvent (ctx, next) {
    const usersClub = await this.tables.Club.findBy('dojoshu_id', ctx.session.loggedInAs.id)
    if (Object.keys(usersClub).length) {
      const eventsClubIDs = await this.tables.Event.getClubs(ctx.params.id, ['id']).then((ids) => ids.map((id) => id.id))
      if (eventsClubIDs.indexOf(usersClub.id) >= 0) {
        const event = {}
        if (ctx.request.body.draft !== undefined) event.draft = ctx.request.body.draft
        if (ctx.request.body.name) event.name = ctx.request.body.name
        if (ctx.request.body.technical_director) event.technical_director = ctx.request.body.technical_director
        if (ctx.request.body.tags) event.tags = JSON.stringify(ctx.request.body.tags)
        if (ctx.request.body.discount_rule) event.discount_rule = ctx.request.body.discount_rule
        if (ctx.request.body.discount_effect) event.discount_effect = ctx.request.body.discount_effect
        if (ctx.request.body.register_start) event.register_start = new Date(ctx.request.body.register_start)
        if (ctx.request.body.register_end) event.register_end = new Date(ctx.request.body.register_end)

        await this.tables.Event.update(ctx.params.id, event, ['id'])

        ctx.body = {
          success: true
        }
      } else {
        ctx.status = 401
        ctx.body = {
          success: false,
          message: 'You are not allowed to edit this event'
        }
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'You are not affiliated with any club'
      }
    }
  }

  /**
   * Create a new event
   * DELETE /events/:id
   * @param {*} ctx
   * @param {*} next
   */
  async deleteEvent (ctx, next) {
    // This will most likley have to deal with users with MULTIPLE clubs, only gets first right now
    const usersClubID = await this.tables.Club.findBy('dojoshu_id', ctx.session.loggedInAs.id).then((club) => club ? club.id : -1)
    const eventsClubIDs = await this.tables.Event.getClubs(ctx.params.id, ['id']).then((ids) => ids.map((id) => id.id))
    if (eventsClubIDs.indexOf(usersClubID) >= 0) {
      await this.tables.Event.atomicDelete(ctx.params.id)

      ctx.body = {
        success: true
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'You are not allowed to edit this event'
      }
    }
  }

  /**
   * Copy an event by ID
   * POST /events/:id/copy
   * @param {*} ctx
   * @param {*} next
   */
  async copyEvent (ctx, next) {
    const event = await this.tables.Event.find(ctx.params.id)
    if (event) {
      const newEvent = await this.tables.Event.copy(ctx.params.id)

      ctx.body = {
        success: true,
        id: newEvent.id
      }
    } else {
      ctx.status = 404
      ctx.body = {
        success: false,
        message: `Event ID #${ctx.params.id} does not exist`
      }
    }
  }
}

export default Event
