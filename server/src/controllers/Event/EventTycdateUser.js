/**
 * Get all registered users for TYCDATE in Event
 * GET /events/:eventID/tycdates/:tydateID/users
 * @param {*} ctx
 * @param {*} next
 */
export async function getUsers (ctx, next) {
  const tycdates = await this.tables.Event.getTycdates(ctx.params.eventID, ['id']).then((tycdates) => {
    return tycdates.filter((tycdate) => tycdate.id === parseInt(ctx.params.tycdateID))
  })
  if (tycdates.length) {
    const users = await this.tables.Tycdate.getUsers(ctx.params.tycdateID, ['username', 'id'])

    ctx.body = {
      success: true,
      users
    }
  } else {
    ctx.status = 404
    ctx.body = {
      success: false,
      message: `Event ID #${ctx.params.eventID} does not contain TYCDATE ID #${ctx.params.tycdateID}`
    }
  }
}

/**
 * Add a user to a TYCDATE registered to an event
 * PUT /events/:eventID/tycdates/:tydateID/users
 * @param {*} ctx
 * @param {*} next
 */
export async function registerUserForTycdate (ctx, next) {
  const valid = await this.tables.Tycdate.isPartOfEvent(ctx.params.tycdateID, ctx.params.eventID)
  if (valid) {
    await this.tables.Tycdate.registerUser(ctx.params.tycdateID, ctx.session.loggedInAs.id)

    ctx.body = {
      success: true
    }
  } else {
    ctx.status = 404
    ctx.body = {
      success: false,
      message: `TYCDATE ID #${ctx.params.tycdateID} is not part of event ID #${ctx.params.eventID}`
    }
  }
}
