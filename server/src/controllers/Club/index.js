import SecureRouter from '../parents/SecureRouter'

import { getImageFromRequest } from '../utils/imageUpload'

import * as ClubNewsletter from './ClubNewsletter'
import * as ClubUser from './ClubUser'
import * as ClubEvent from './ClubEvent'

class Club extends SecureRouter {
  constructor () {
    super('/clubs')

    // Self
    this._get('/', this.getClubs)
    this._get('/:id', this.getClub)
    this._post('/', this.postClub)
    this._put('/:id/avatar', this.putAvatar)

    // Club->Newsletter
    this._post('/:id/newsletter', ClubNewsletter.uploadNewsletter)

    // Club->Event
    this._get('/:id/events', ClubEvent.getClubEvents)

    // Club->User
    this._get('/:id/users', ClubUser.getClubUsers)
    this._get('/:id/users/:paid', ClubUser.getFilteredClubUsers)
    this._get('/:id/users/:paid/count', ClubUser.countFilteredClubUsers)
    this._post('/:clubID/users/:userID', ClubUser.addUserToClub)
    this._put('/:clubID/users/:userID', ClubUser.setUserEditables)

    this._post('/:id/requests', ClubUser.requestToJoinClub)
    this._delete('/:id/requests/:username', ClubUser.deleteRequestToJoinClub)
    this._get('/:id/requests', ClubUser.getJoinRequests)
    this._get('/:id/requests/count', ClubUser.countJoinRequests)
  }

  async ownsClub (user, clubID) {
    const club = await this.tables.Club.find(clubID)
    return club ? user.id === club.dojoshu_id : false
  }

  /**
   * Get all clubs
   * GET /clubs
   * @param {*} ctx
   * @param {*} next
   */
  async getClubs (ctx, next) {
    const clubs = await this.tables.Club.select('*')

    ctx.body = {
      success: true,
      clubs
    }
  }

  /**
   * Get club data from club id
   * GET /clubs/:id
   * @param {*} ctx
   * @param {*} next
   */
  async getClub (ctx, next) {
    const club = await this.tables.Club.find(ctx.params.id)

    ctx.body = {
      success: true,
      club
    }
  }

  /**
   * Club setup
   * POST /clubs
   * @param {*} ctx
   * @param {*} next
   */
  async postClub (ctx, next) {
    const clubNameAvailable = (await this.tables.Club.unique('name', ctx.request.body.name))

    if (clubNameAvailable) {
      let id = await (this.tables.Club.insert({
        name: ctx.request.body.name,
        description: ctx.request.body.description,
        dojoshu_id: ctx.session.loggedInAs.id
      }, ['id']).then(async (res) => {
        let id = res.id
        await this.tables.User.attachClub(ctx.session.loggedInAs.id, id)
        return id
      }))

      ctx.body = {
        success: true,
        id
      }
    } else {
      ctx.status = 409
      ctx.body = {
        success: false,
        message: 'Club name is already taken.'
      }
    }
  }

  /**
   * Upload a new profile picture for the club
   * PUT /clubs/:id/avatar
   * @param {*} ctx
   * @param {*} next
   */
  async putAvatar (ctx, next) {
    const clubExists = await this.tables.Club.exists(ctx.params.id)

    if (clubExists) {
      const club = await this.tables.Club.find(ctx.params.id)
      const isClubDojoshu = club.dojoshu_id === ctx.session.loggedInAs.id
      if (isClubDojoshu) {
        const url = getImageFromRequest(ctx, 'avatars', 'event-avatar')
        if (url) {
          await this.tables.Club.updateWhere('id', club.id, {
            avatar: url
          })

          ctx.body = {
            success: true,
            url
          }
        }
      } else {
        ctx.status = 401
        ctx.body = {
          success: false,
          message: `You are not the dojoshu of this club`
        }
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: `That club does not exist`
      }
    }
  }
}

export default Club
