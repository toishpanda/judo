import { UserMessage } from '../../utils/UserMessage'

async function validClub (ctx, _this, clubID, callback) {
  const clubExists = await _this.tables.Club.exists(clubID)
  if (clubExists) {
    await callback()
  } else {
    ctx.status = 404
    ctx.body = {
      success: false,
      message: `Club ID #${clubID} does not exist`
    }
  }
}

async function validDojoshu (ctx, _this, clubID, userID, callback) {
  await validClub(ctx, _this, clubID, async function () {
    const isClubDojoshu = (await _this.tables.Club.find(clubID)).dojoshu_id === userID
    if (isClubDojoshu) {
      await callback()
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'You are not the dojoshu of this club'
      }
    }
  })
}

/**
 * Get users registered to club
 * GET /club/:id/users
 * @param {*} ctx
 * @param {*} next
 */
export async function getClubUsers (ctx, next) {
  await validClub(ctx, this, ctx.params.id, async () => {
    const users = await this.tables.Club.getUsers(ctx.params.id, ['id', 'firstname', 'lastname', 'username', 'email', 'active'])
    const requests = await this.tables.Club.getJoinRequests(ctx.params.id, ['id', 'firstname', 'lastname', 'username', 'email', 'active'])

    ctx.body = {
      success: true,
      users,
      requests
    }
  })
}

/**
 * Add user to club if logged in user is Dojoshu of club
 * POST /club/:clubID/users/:userID
 * @param {*} ctx
 * @param {*} next
 */
export async function addUserToClub (ctx, next) {
  await validDojoshu(ctx, this, ctx.params.clubID, ctx.session.loggedInAs.id, async () => {
    const userHasPendingJoinRequest = await this.tables.Club.userHasPendingJoinRequest(ctx.params.clubID, ctx.params.userID)
    if (userHasPendingJoinRequest) {
      await this.tables.Club.attachUser(ctx.params.clubID, ctx.params.userID)
      await this.tables.Club.deleteJoinRequest(ctx.params.clubID, ctx.params.userID)

      const clubName = await this.tables.Club.find(ctx.params.clubID, ['name']).then((res) => res.name)

      const message = new UserMessage(parseInt(ctx.params.userID), `Welcome to ${clubName}!`, `${ctx.session.loggedInAs.username} has accepted you into the ${clubName}`)
      await message.send(ctx.session.loggedInAs.id) // Maybe this should send from the 'judo ontario system' instead?

      ctx.body = {
        success: true
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'User has not requested to join your club'
      }
    }
  })
}

/**
 * Set dojoshu editables on club members
 * PUT /club/:clubID/users/:userID
 * @param {*} ctx
 * @param {*} next
 */
export async function setUserEditables (ctx, next) {
  await validDojoshu(ctx, this, ctx.params.clubID, ctx.session.loggedInAs.id, async () => {
    const isUsersDojoshu = await this.tables.User.isUsersDojoshu(ctx.session.loggedInAs.id, ctx.params.userID)
    if (isUsersDojoshu) {
      let newUserData = {}
      if (ctx.request.body.belt) newUserData.belt = ctx.request.body.belt
      if (ctx.request.body.judoontario_number) newUserData.judoontario_number = ctx.request.body.judoontario_number

      await this.tables.User.update(ctx.params.userID, newUserData)

      ctx.body = {
        success: true
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'You are not this user\'s Dojoshu'
      }
    }
  })
}

/**
 * Create a join request for logged in user to club
 * POST /club/:id/requests
 * @param {*} ctx
 * @param {*} next
 */
export async function requestToJoinClub (ctx, next) {
  const clubExists = await this.tables.Club.exists(ctx.params.id)
  if (clubExists) {
    await this.tables.Club.createJoinRequest(ctx.params.id, ctx.session.loggedInAs.id)
      .then(async () => {
        const dojoshuID = (await this.tables.Club.find(ctx.params.id)).dojoshu_id
        const message = new UserMessage(dojoshuID, 'New club join request', `${ctx.session.loggedInAs.username} would like to join your club! Check your club requests to add them.`)
        await message.send(ctx.session.loggedInAs.id) // Maybe this should send from the 'judo ontario system' instead?

        ctx.body = {
          success: true
        }
      })
      .catch((err) => {
        ctx.status = 400
        ctx.body = {
          success: false,
          message: err.message
        }
      })
  } else {
    ctx.status = 404
    ctx.body = {
      success: false,
      message: `Club ID #${ctx.params.id} does not exist`
    }
  }
}

/**
 * Delete a join request for a user if the logged in user is the dojoshu
 * DELETE /club/:id/requests/:username
 * @param {*} ctx
 * @param {*} next
 */
export async function deleteRequestToJoinClub (ctx, next) {
  const clubExists = await this.tables.Club.exists(ctx.params.id)
  if (clubExists) {
    const dojoshu = await this.tables.Club.find(ctx.params.id, ['dojoshu_id']).then((res) => res.dojoshu_id)
    if (dojoshu === ctx.session.loggedInAs.id) {
      const user = await this.tables.User.findBy('username', ctx.params.username)
      if (Object.keys(user).length) {
        const userHasPendingJoinRequest = await this.tables.Club.userHasPendingJoinRequest(ctx.params.id, user.id)
        if (userHasPendingJoinRequest) {
          await this.tables.Club.deleteJoinRequest(ctx.params.id, user.id)

          ctx.body = {
            success: true
          }
        } else {
          ctx.status = 404
          ctx.body = {
            success: false,
            message: `User '${ctx.params.username}' does not have a pending join request.`
          }
        }
      } else {
        ctx.status = 404
        ctx.body = {
          success: false,
          message: `User '${ctx.params.username}' cannot be found`
        }
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: `You are not the dojoshu of this club (#${ctx.params.id})`
      }
    }
  } else {
    ctx.status = 404
    ctx.body = {
      success: false,
      message: `Club ID #${ctx.params.id} does not exist`
    }
  }
}

/**
 * Get array of join requests for this club
 * GET /club/:id/requests
 * @param {*} ctx
 * @param {*} next
 */
export async function getJoinRequests (ctx, next) {
  const clubExists = await this.tables.Club.exists(ctx.params.id)

  if (clubExists) {
    const isClubDojoshu = (await this.tables.Club.find(ctx.params.id)).dojoshu_id === ctx.session.loggedInAs.id
    if (isClubDojoshu) {
      const users = await this.tables.Club.getJoinRequests(ctx.params.id, ['*'])

      ctx.body = {
        success: true,
        users
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'You are not the dojoshu of this club'
      }
    }
  } else {
    ctx.status = 404
    ctx.body = {
      success: false,
      message: `Club ID #${ctx.params.id} does not exist`
    }
  }
}

/**
 * Count number of join requests for this club
 * GET /club/:id/requests
 * @param {*} ctx
 * @param {*} next
 */
export async function countJoinRequests (ctx, next) {
  const clubExists = await this.tables.Club.exists(ctx.params.id)

  if (clubExists) {
    const isClubDojoshu = (await this.tables.Club.find(ctx.params.id)).dojoshu_id === ctx.session.loggedInAs.id
    if (isClubDojoshu) {
      const total = await this.tables.Club.countJoinRequests(ctx.params.id).then(res => parseInt(res))

      ctx.body = {
        success: true,
        total
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'You are not the dojoshu of this club'
      }
    }
  } else {
    ctx.status = 404
    ctx.body = {
      success: false,
      message: `Club ID #${ctx.params.id} does not exist`
    }
  }
}

/**
 * Get array of paid/unpaid users in club
 * GET /club/:id/users/count/:paid
 * @param {*} ctx
 * @param {*} next
 */
export async function getFilteredClubUsers (ctx, next) {
  const clubExists = await this.tables.Club.exists(ctx.params.id)

  if (clubExists) {
    const isClubDojoshu = (await this.tables.Club.find(ctx.params.id)).dojoshu_id === ctx.session.loggedInAs.id
    if (isClubDojoshu) {
      const users = await this.db('clubs_users')
        .join('users', 'clubs_users.user_id', '=', 'users.id')
        .where({
          active: ctx.params.paid === 'paid',
          club_id: ctx.params.id
        })

      ctx.body = {
        success: true,
        users
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'You are not the dojoshu of this club'
      }
    }
  } else {
    ctx.status = 404
    ctx.body = {
      success: false,
      message: `Club ID #${ctx.params.id} does not exist`
    }
  }
}

/**
 * Get count of paid/unpaid users in club
 * GET /club/:id/users/count/:paid
 * @param {*} ctx
 * @param {*} next
 */
export async function countFilteredClubUsers (ctx, next) {
  const clubExists = await this.tables.Club.exists(ctx.params.id)

  if (clubExists) {
    const isClubDojoshu = (await this.tables.Club.find(ctx.params.id)).dojoshu_id === ctx.session.loggedInAs.id
    if (isClubDojoshu) {
      const total = await this.db('clubs_users')
        .join('users', 'clubs_users.user_id', '=', 'users.id')
        .where({
          active: ctx.params.paid === 'paid',
          club_id: ctx.params.id
        })
        .count()

      ctx.body = {
        success: true,
        total: parseInt(total[0].count)
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'You are not the dojoshu of this club'
      }
    }
  } else {
    ctx.status = 404
    ctx.body = {
      success: false,
      message: `Club ID #${ctx.params.id} does not exist`
    }
  }
}
