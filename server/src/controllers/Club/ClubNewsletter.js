import fs from 'fs'
import path from 'path'
import { UserMessage } from '../../utils/UserMessage'

/**
 * Upload Newsletter to club
 * POST /club/:id/newsletter
 * @param {*} ctx
 * @param {*} next
 */
export async function uploadNewsletter (ctx, next) {
  if (await this.ownsClub(ctx.session.loggedInAs, ctx.params.id)) {
    let now = new Date()
    const file = ctx.request.body.files.newsletter
    const reader = fs.createReadStream(file.path)
    const stream = fs.createWriteStream(path.join(__dirname, '../../../static/newsletters/newsletter-' + (now.toISOString()) + '-' + ctx.params.id))
    reader.pipe(stream)

    await this.tables.Newsletter.insert({
      name: ctx.request.body.name ? ctx.request.body.name : 'Untitled Newsletter',
      created_at: now,
      updated_at: now,
      pdf: stream.path,
      club_id: ctx.params.id
    })

    const clubUserIDs = await this.tables.Club.getUsers(ctx.params.id, ['id']).then((users) => users.map((user) => user.id))

    const messages = new UserMessage(clubUserIDs, `Newsletter: ${ctx.request.body.name ? ctx.request.body.name : 'Untitled Newsletter'}`, `You can read the newsletter <a href="http://judoontario.ca/${stream.path}">here</a>`)
    await messages.send(ctx.session.loggedInAs.id)

    ctx.body = { success: true, path: stream.path }
  } else {
    ctx.status = 401
    ctx.body = {
      'success': false,
      'message': 'User does not own or manage this club.'
    }
  }
}
