async function validClub (ctx, _this, clubID, callback) {
  const clubExists = await _this.tables.Club.exists(clubID)
  if (clubExists) {
    await callback()
  } else {
    ctx.status = 404
    ctx.body = {
      success: false,
      message: `Club ID #${clubID} does not exist`
    }
  }
}

async function validDojoshu (ctx, _this, clubID, userID, callback) {
  await validClub(ctx, _this, clubID, async function () {
    const isClubDojoshu = (await _this.tables.Club.find(clubID)).dojoshu_id === userID
    if (isClubDojoshu) {
      await callback()
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'You are not the dojoshu of this club'
      }
    }
  })
}

/**
 * Get all events associated with club
 * GET /club/:id/events
 * @param {*} ctx
 * @param {*} next
 */
export async function getClubEvents (ctx, next) {
  await validDojoshu(ctx, this, ctx.params.id, ctx.session.loggedInAs.id, async () => {
    const events = await this.tables.Club.getClubsEvents(ctx.params.id)

    ctx.body = {
      success: true,
      events
    }
  })
}
