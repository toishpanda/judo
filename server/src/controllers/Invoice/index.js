import SecureRouter from '../parents/SecureRouter'

import InvoiceAPI from '../../utils/Invoice.js'

class Invoice extends SecureRouter {
  constructor () {
    super('/invoices')

    // Self
    this._get('/', this.getInvoices)
    this._post('/', this.postInvoice)

    // this._get()
  }

  /**
   * Get a list of all the logged-in user's invoices
   * GET /invoices/
   * @param {*} ctx
   * @param {*} next
   */
  async getInvoices (ctx, next) {
    const invoices = await this.tables.Invoice.getInvoicesFor('users', ctx.session.loggedInAs.id)

    ctx.body = {
      success: true,
      invoices
    }
  }

  /**
   * Create a new invoice with restrictions based on the logged-in user's account
   * POST /invoices/
   * ?to = the ID of the user to send to
   * @TODO get rid of the awful
   * @param {*} ctx
   * @param {*} next
   */
  async postInvoice (ctx, next) {
    const user = await this.tables.User.findBy('username', ctx.request.body.to)
    if (await this.tables.User.isUsersDojoshu(ctx.session.loggedInAs.id, user.id)) {
      if (Object.keys(user).length) {
        const invoice = new InvoiceAPI()
          .fromUser(ctx.session.loggedInAs.id)
          .toUser(user.id)
          .appendItem(this.tables.Invoice.CONSTS.SPECIALFEE, 'Test', 'Just a test charge', 0)
        await invoice.commit()
        console.log(await invoice.preload())

        ctx.body = {
          success: true,
          user: user
        }
      } else {
        ctx.status = 404
        ctx.body = {
          success: false,
          message: `Username "${ctx.request.body.to}" was not found`
        }
      }
    } else {
      ctx.status = 403
      ctx.body = {
        success: false,
        message: `You are not ${ctx.request.body.to}'s dojoshu.`
      }
    }
  }
}

export default Invoice
