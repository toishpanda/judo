import BasicRouter from './BasicRouter'

class SecureRouter extends BasicRouter {
  constructor (prefix) {
    super(prefix)

    this.router.use(async function (ctx, next) {
      if (ctx.session.loggedInAs) return next()
      else {
        ctx.status = 401
        ctx.body = {
          success: false,
          message: 'Your session is not logged in.'
        }
      }
    })
  }
}

export default SecureRouter
