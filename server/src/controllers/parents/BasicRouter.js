import KoaRouter from 'koa-router'

class BasicRouter {
  constructor (prefix) {
    this.router = new KoaRouter({ prefix: '/api' + prefix })
    this.routeMap = []
  }

  _commit () {
    this.routeMap.forEach((route) => {
      this.router[route.method](route.path, route.fn.bind(this))
    })
  }

  _get (path, fn) {
    this.routeMap.push({ method: 'get', path, fn })
  }

  _post (path, fn) {
    this.routeMap.push({ method: 'post', path, fn })
  }

  _delete (path, fn) {
    this.routeMap.push({ method: 'delete', path, fn })
  }

  _put (path, fn) {
    this.routeMap.push({ method: 'put', path, fn })
  }
}

export default BasicRouter
