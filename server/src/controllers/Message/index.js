import SecureRouter from '../parents/SecureRouter'
import { UserMessage } from '../../utils/UserMessage'

class Message extends SecureRouter {
  constructor () {
    super('/messages')

    // Self
    this._get('/', this.getMessages)
    this._get('/:id', this.getMessage)
    this._post('/', this.sendMessage)
    this._delete('/:id', this.deleteMessage)

    this._get('/filter/:mode', this.getFilteredMessages)
    this._get('/filter/:mode/count', this.countFilteredMessages)
  }

  /**
   * Get all of logged in user's messages
   * GET /messages
   * @param {*} ctx
   * @param {*} next
   */
  async getMessages (ctx, next) {
    let messages = await this.tables.Message.getUsersMessages(ctx.session.loggedInAs.id, 'all', ['messages.created_at', 'messages.subject', 'messages.from', 'messages.id'])

    messages = await Promise.all(messages.map(async (message) => {
      let messageUnread = await this.tables.Message.isUnread(message.id, ctx.session.loggedInAs.id)
      let messageFrom = await this.tables.User.find(message.from)
      return Object.assign(message, { unread: messageUnread, from: messageFrom })
    }))

    ctx.body = {
      success: true,
      messages
    }
  }

  /**
   * Get one message from message ID and mark it as read
   * GET /messages/:id
   * @param {*} ctx
   * @param {*} next
   */
  async getMessage (ctx, next) {
    if (await this.tables.Message.isSentTo(ctx.params.id, ctx.session.loggedInAs.id)) {
      let message = await this.tables.Message.find(ctx.params.id)
      message.from = await this.tables.User.find(message.from)

      await this.tables.Message.markAsRead(ctx.params.id, ctx.session.loggedInAs.id)

      ctx.body = {
        success: true,
        message
      }
    } else {
      ctx.body = {
        success: false,
        message: 'This message was not sent to you'
      }
    }
  }

  /**
   * Send a message
   * POST /messages
   * @param {*} ctx
   * @param {*} next
   */
  async sendMessage (ctx, next) {
    if (ctx.request.body.content.length > 1024) {
      ctx.status = 413
      ctx.body = {
        success: false,
        message: `Your message is too long, it must be shorter than 1024 characters. Your message is ${ctx.request.body.content.length} characters long.`
      }
    } else if (ctx.request.body.subject.length > 255) {
      ctx.status = 413
      ctx.body = {
        success: false,
        message: `Your subject is too long, it must be shorter than 255 characters. Your subject is ${ctx.request.body.subject.length} characters long.`
      }
    } else {
      const message = new UserMessage(ctx.request.body.to, ctx.request.body.subject, ctx.request.body.content)
      const success = await message.send(ctx.session.loggedInAs.id)

      if (success) {
        ctx.body = {
          success: true
        }
      } else {
        ctx.status = 422
        ctx.body = {
          success: false,
          message: 'A user you supplied does not exist.'
        }
      }
    }
  }

  /**
   * Delete a message from inbox
   * DELETE /messages
   * @param {*} ctx
   * @param {*} next
   */
  async deleteMessage (ctx, next) {
    await this.tables.Message.deleteFromUsersInbox(ctx.params.id, ctx.session.loggedInAs.id)

    ctx.body = {
      success: true
    }
  }

  /**
   * Get a filtered list of logged in user's messages
   * GET /messages/filter/:mode
   * @param {*} ctx
   * @param {*} next
   */
  async getFilteredMessages (ctx, next) {
    let messages = await this.tables.Message.getUsersMessages(ctx.session.loggedInAs.id, ctx.params.mode, ['messages.created_at', 'messages.subject', 'messages.from', 'messages.id'])

    messages = await Promise.all(messages.map(async (message) => {
      let messageUnread = await this.tables.Message.isUnread(message.id, ctx.session.loggedInAs.id)
      let messageFrom = await this.tables.User.find(message.from)
      return Object.assign(message, { unread: messageUnread, from: messageFrom })
    }))

    ctx.body = {
      success: true,
      messages
    }
  }

  /**
   * Count logged-in user's read/unread/all messages
   * GET /user/messages/filter/:mode/count
   * @param {*} ctx
   * @param {*} next
   */
  async countFilteredMessages (ctx, next) {
    const total = await this.tables.Message.countUsersMessages(ctx.session.loggedInAs.id, ctx.params.mode)

    ctx.body = {
      success: true,
      total
    }
  }
}

export default Message
