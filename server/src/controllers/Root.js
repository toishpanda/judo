import BasicRouter from './parents/BasicRouter'

class Root extends BasicRouter {
  constructor () {
    super('/')

    // Self
    this._get('/', this.getHome)
  }

  getHome (ctx, next) {
    ctx.body = '200 OK ✔'
  }
}

export default Root
