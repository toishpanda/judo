// ---
// * This file is NOT the index router! (e.g. www.example.com/)
// * "Root.js" represents the index route.
// ---

import KoaCompose from 'koa-compose'
import db from '../db.js'
import tables from '../tables'

// --------------------------
// Import routers here
import Root from './Root'
import User from './User'
import Auth from './Auth'
import Club from './Club'
import Event from './Event'
import Message from './Message'
import Invoice from './Invoice'

// List routers here
const routers = [ Root, User, Auth, Club, Event, Message, Invoice ]
// --------------------------

// This is the last line of defence to prevent NOTHING
// going back when an error occurs somewhere along the middleware chain.
// Ideally, it should never be used.
async function ErrorHandler (ctx, next) {
  try {
    await next()
  } catch (err) {
    ctx.status = err.status || 500
    if (process.env.KOA_SEND_ERRORS) {
      ctx.body = {
        success: false,
        message: err
      }
    } else {
      ctx.body = {
        success: false,
        message: 'There was an internal error. The issue has been logged.'
      }
    }
    ctx.app.emit('error', err, ctx)
  }
}

/** Compiles all of the routers together into one Koa middleware object */
function generateRoutes () {
  const routerMiddleware = routers.map((router) => {
    // Basically `new` keyword
    return Reflect.construct(router, [])
  }).reduce((routes, routerInstance) => {
    // Assign routes to resource functions
    routerInstance._commit()

    // Inject Knex interface
    routerInstance.db = db
    routerInstance.tables = tables(db)

    // Join all the routes together
    return routes.concat([routerInstance.router.routes()])
  }, [ErrorHandler])

  return KoaCompose(routerMiddleware)
}

export default generateRoutes
