import BasicRouter from '../parents/BasicRouter'

import bcrypt from 'bcrypt'
import Postmark from 'postmark'
import { Token, getToken } from '../../utils/Token.js'

// This is used to supply data to the frontend about user. Must be the same for both /check + /login
import { supplyUserObject } from '../utils/userObject'

class Auth extends BasicRouter {
  constructor () {
    super('/auth')

    // Self
    this._get('/username-available/:username', this.getUsernameAvailable)
    this._post('/login', this.getLogin)
    this._get('/logout', this.getLogout)
    this._get('/check', this.getCheck)
    this._post('/password-reset', this.postPasswordReset)
    this._put('/password-reset/:key', this.applyPasswordReset)
  }

  async usernameAvailable (username) {
    return this.tables.User.unique('username', username)
  }

  /**
   * Returns if the passed in username is availiable
   * GET /auth/username-available/:username
   * @param {*} ctx
   * @param {*} next
   */
  async getUsernameAvailable (ctx, next) {
    ctx.body = { available: await this.usernameAvailable(ctx.params.username) }
  }

  /**
   * User Login
   * POST /auth/login
   * @param {*} ctx
   * @param {*} next
   */
  async getLogin (ctx, next) {
    const userExists = await this.tables.User.unique('username', ctx.request.body.username)

    if (!userExists) {
      const user = await this.tables.User.findByUnsafe('username', ctx.request.body.username)
      const loginSucess = await bcrypt.compare(ctx.request.body.password, user.password)

      if (loginSucess) {
        ctx.session.loggedInAs = user
        ctx.body = {
          success: true,
          user: await supplyUserObject(this.tables, user)
        }
        return true
      }
    }

    ctx.status = 401
    ctx.session.loggedInAs = null
    ctx.body = {
      'success': false,
      'message': 'Password is incorrect.'
    }

    return false
  }

  /**
   * User Check if logged in
   * POST /auth/check
   * @param {*} ctx
   * @param {*} next
   */
  async getCheck (ctx, next) {
    if (Object.keys(ctx.session.loggedInAs || {}).length) {
      await this.tables.User.refreshUserCache(ctx)
      ctx.body = {
        loggedIn: true,
        user: await supplyUserObject(this.tables, ctx.session.loggedInAs)
      }
    } else {
      ctx.body = {
        loggedIn: false
      }
    }
  }

  /**
   * User Logout
   * POST /auth/logout
   * @param {*} ctx
   * @param {*} next
   */
  async getLogout (ctx, next) {
    ctx.session.loggedInAs = null
    ctx.body = { success: true }
  }

  /**
   * User Reset password using email
   * POST /auth/password-reset
   * @param {*} ctx
   * @param {*} next
   */
  async postPasswordReset (ctx, next) {
    const valid = await this.tables.User.select(1).where('email', '=', ctx.request.body.email || '').first().then((user) => user !== undefined)
    if (valid && ctx.request.body.email) {
      const users = await this.tables.User.select('*').where('email', '=', ctx.request.body.email)
      const links = await Promise.all(users.map(async (user) => {
        const token = new Token('password-reset', { email: ctx.request.body.email, user: user.id })
        await token.register()
        return token.makeLink('Reset the password for ' + user.username)
      }))
      let message = '<h2>Judo Ontario</h2>'
      message += `<p>Someone has requested a password reset for all Judo Ontario accounts registered to this email address. (${ctx.request.body.email}) Use the links below to reset the password for each Judo Ontario account. <strong>If you did not request this password reset, you can safely ignore this email.</strong></p></hr>`
      message += links.reduce((acc, link) => {
        return `${acc}<br/>${link}`
      }, '')

      const postmarkClient = new Postmark.Client('84e41bcc-9361-4d6b-abaf-3961f26521e2')
      postmarkClient.sendEmail({
        From: 'website@judoontario.ca',
        To: ctx.request.body.email,
        Subject: 'Judo Ontario Password Reset',
        HTMLBody: message
      })

      ctx.body = {
        success: true
      }
    } else {
      ctx.status = 404
      ctx.body = {
        success: false,
        message: 'That email is not registered to a user.'
      }
    }
  }

  /**
   * User Reset password using key passed in as a property
   * PUT /auth/password-reset/:key
   * @param {*} ctx
   * @param {*} next
   */
  async applyPasswordReset (ctx, next) {
    const token = await getToken(ctx.params.key)
    if (token) {
      await token.unregister()
      await this.tables.User.find(token.data.user).update('password', await bcrypt.hash(ctx.request.body.password, 10))
      ctx.body = {
        success: true
      }
    } else {
      ctx.status = 404
      ctx.body = {
        success: false,
        message: 'That authentication token does not exist'
      }
    }
  }
}

export default Auth
