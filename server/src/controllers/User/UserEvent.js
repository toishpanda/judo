/**
 * Get user's registered events
 * GET /user/:username/events
 * @param {*} ctx
 * @param {*} next
 */
export async function getUsersEvents (ctx, next) {
  const user = await this.tables.User.findBy('username', ctx.params.username)

  if (Object.keys(user).length) {
    const events = await this.tables.User.getRegisteredEvents(user.id, ['name'])

    ctx.body = {
      success: true,
      events
    }
  } else {
    ctx.status = 404
    ctx.body = {
      success: false,
      message: `User with username "${ctx.params.username}" was not found.`
    }
  }
}
