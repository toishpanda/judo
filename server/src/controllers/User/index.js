import BasicRouter from '../parents/BasicRouter'

import * as UserEvent from './UserEvent'

import { supplyUserObject } from '../utils/userObject'
import { getImageFromRequest } from '../utils/imageUpload'

import bcrypt from 'bcrypt'
import _without from 'lodash.without'
import validate from 'validate.js'

class User extends BasicRouter {
  constructor () {
    super('/users')

    // Self
    this._get('/', this.getUsers) // Get all
    this._get('/:username', this.getUser) // Get one
    this._put('/:username', this.putUser) // Edit
    this._put('/:username/avatar', this.putAvatar) // Upload Avatar
    this._post('/', this.postUser) // Create

    // User->Events
    this._get('/:username/events', UserEvent.getUsersEvents)
  }

  async usernameAvailable (username) {
    return this.tables.User.unique('username', username)
  }

  /**
   * Get all users, newest first, oldest last
   * GET /users
   * @param {*} ctx
   * @param {*} next
   */
  async getUsers (ctx, next) {
    if (ctx.query.q) {
      const query = `%${ctx.query.q.replace(/([^a-zA-Z\d\s])/gi, '').split(' ').join('|')}%`
      console.log(query)
      const users = await this.tables.User.select([
        'id',
        'username',
        'firstname',
        'lastname',
        'avatar'
      ]).where('firstname', 'ILIKE', query)
        .orWhere('lastname', 'ILIKE', query)
        .orderBy('id', 'desc')
      ctx.body = {
        success: true,
        users
      }
    } else {
      const users = await this.tables.User.select(['id', 'username']).orderBy('id', 'desc')
      ctx.body = {
        success: true,
        users
      }
    }
  }

  /**
   * Get a user's public profile
   * GET /users/:username
   * @param {*} ctx
   * @param {*} next
   */
  async getUser (ctx, next) {
    const user = await this.tables.User.findBy('username', ctx.params.username)

    if (Object.keys(user).length) {
      ctx.body = {
        success: true,
        user: await supplyUserObject(this.tables, user)
      }
    } else {
      ctx.status = 404
      ctx.body = {
        success: false,
        message: `Username "${ctx.params.username}" was not found`
      }
    }
  }

  /**
   * Update a user with new data
   * PUT /users/:username
   * @param {*} ctx
   * @param {*} next
   */
  async putUser (ctx, next) {
    if (ctx.session.loggedInAs.username === ctx.params.username) {
      if (ctx.request.body) {
        // Filter out any bad input
        const columns = _without(await this.tables.User.getColumns(), 'id', 'username', 'password', 'admin', 'active', 'belt')
        const userID = await this.tables.User.findBy('username', ctx.params.username).then((user) => user.id)

        await this.tables.User.update(userID, Object.keys(ctx.request.body).reduce((acc, key) => {
          return (columns.includes(key) ? Object.assign(acc, { [key]: ctx.request.body[key] }) : acc)
        }, {}))

        ctx.body = {
          success: true
        }
      } else {
        ctx.status = 400
        ctx.body = {
          success: false,
          message: `You must pass in user data`
        }
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: `You are not logged in as "${ctx.params.username}"`
      }
    }
  }

  /**
   * Upload a new profile picture
   * PUT /users/:username/avatar
   * @param {*} ctx
   * @param {*} next
   */
  async putAvatar (ctx, next) {
    if (ctx.session.loggedInAs) {
      if (ctx.params.username && (ctx.session.loggedInAs.username === ctx.params.username)) {
        const url = getImageFromRequest(ctx, 'avatars', 'user-avatar')
        if (url) {
          await this.tables.User.updateWhere('id', ctx.session.loggedInAs.id, {
            avatar: url
          })

          await this.tables.User.refreshUserCache(ctx)

          ctx.body = {
            success: true,
            url
          }
        }
      } else {
        ctx.status = 401
        ctx.body = {
          success: false,
          message: `You are not logged in as "${ctx.params.username}"`
        }
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: `You are not logged in`
      }
    }
  }

  /**
   * User Signup
   * POST /users
   * @param {*} ctx
   * @param {*} next
   */
  async postUser (ctx, next) {
    if (await this.usernameAvailable(ctx.request.body.username)) {
      const user = {
        firstname: ctx.request.body.firstname,
        lastname: ctx.request.body.lastname,
        username: ctx.request.body.username,
        password: await bcrypt.hash(ctx.request.body.password, 10),
        email: ctx.request.body.email,
        active: false,
        admin: false
      }

      const constraints = {
        firstname: { presence: { allowEmpty: false }, length: { maximum: 24 } },
        lastname: { presence: { allowEmpty: false }, length: { maximum: 24 } },
        username: { presence: { allowEmpty: false }, length: { maximum: 24 } },
        password: { length: { minimum: 6 } },
        email: { presence: { allowEmpty: false }, email: true, length: { maximum: 35 } }
      }

      const errors = validate(user, constraints)

      if (errors) {
        ctx.status = 422
        ctx.body = {
          success: false,
          message: Object.keys(errors).reduce((acc, itemname) => {
            return `${acc}${errors[itemname].reduce((acc, error) => acc + error + '. ')}. `
          }, '')
        }
      } else {
        await this.tables.User.insert(Object.assign(user, { avatar: 'defaults/avatar.png' }))

        ctx.body = {
          success: true
        }
      }
    } else {
      ctx.status = 409
      ctx.body = {
        success: false,
        message: 'Username \'' + ctx.request.body.username + '\' is taken.'
      }
    }
  }
}

export default User
