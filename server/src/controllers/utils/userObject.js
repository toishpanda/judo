/**
 * Add extra information about the user to a user object. Used when hydrating the frontend's state.
 * @param {Object} tables - The table manager object from BasicRouter (normally this.tables)
 * @param {Object} user - Should be a SINGLE user entry from Knex. (result of a select.)
 */
export async function supplyUserObject (tables, user) {
  if (Object.keys(user).length) {
    const club = await tables.User.getClubs(user.id, ['*'])

    let events = await tables.User.getRegisteredEvents(user.id, ['*'])
    const eventClubs = await Promise.all(events.map((res) => {
      return tables.Event.getClubs(res.event.id, ['name', 'id', 'avatar'])
    }))
    events = events.map((event, index) => Object.assign(event, { clubs: eventClubs[index] }))

    const clubWithDojoshu = await Promise.all(club.map(async (club) => {
      const dojoshu = await tables.User.find(club.dojoshu_id)
      return Object.assign(club, { dojoshu })
    }))

    return {
      created_at: user.created_at,
      username: user.username,
      admin: !!user.admin,
      email: user.email,
      avatar: user.avatar,
      firstname: user.firstname,
      lastname: user.lastname,
      belt: user.belt,
      judo_ontario_number: user.judo_ontario_number,
      judo_canada_number: user.judo_canada_number,
      clubs: clubWithDojoshu || null,
      events: events || null
    }
  }

  return false
}
