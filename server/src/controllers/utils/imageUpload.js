import fs from 'fs'
import path from 'path'
import mime from 'mime-types'

function makeURLSafe (str) {
  return str.replace(/[^a-zA-Z0-9-]/g, '')
}

/**
 * Capture an uploaded image from a multipart form request and make sure it meets some basic condititons
 * @param {Object} ctx - Koa context
 * @param {Object} group - Upload type (avatars/newletters/etc)
 * @param {Object} name - The name of the propert inside the form request
 */
export function getImageFromRequest (ctx, group, name) {
  let now = new Date()
  const file = ctx.request.body.files.avatar

  const allowed = [
    'image/gif',
    'image/png',
    'image/jpeg',
    'image/bmp',
    'image/webp'
  ]

  if (allowed.indexOf(file.type) >= 0) {
    const reader = fs.createReadStream(file.path)
    const extension = mime.extension(file.type)
    const url = `${makeURLSafe(group)}/${makeURLSafe(name)}-${makeURLSafe(now.toISOString())}-${makeURLSafe(ctx.session.loggedInAs.username)}.${makeURLSafe(extension)}`
    const stream = fs.createWriteStream(path.join(__dirname, '../../../static/' + url))
    reader.pipe(stream)
    return url
  } else {
    ctx.status = 415
    ctx.body = {
      success: false,
      message: `File type "${file.type}" is not allowed.`
    }
    return false
  }
}
