import BasicTable from './parents/BasicTable'

class Tycdategroup extends BasicTable {
  constructor (db) {
    super('tycdategroups', db)
  }

  // -- New Functions -- //

  getTycdates (id, returning) {
    return this.getRelationVia(id, 'event', {
      table: 'tycdategroups_tycdates',
      sourceCol: 'tycdategroups_id',
      goalCol: 'event_id'
    }, returning)
  }

  // -- Overloaded Functions -- //
  // ---------- None ---------- //
}

export default Tycdategroup
