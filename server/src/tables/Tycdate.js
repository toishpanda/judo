import BasicTable from './parents/BasicTable'

class Tycdate extends BasicTable {
  constructor (db) {
    super('tycdates', db)
  }

  // -- New Functions -- //

  getEvent (id, returning) {
    return this.getRelationVia(id, 'event', {
      table: 'events_tycdates',
      sourceCol: 'tycdate_id',
      goalCol: 'event_id'
    }, returning)
  }

  isPartOfEvent (tycdateID, eventID) {
    return this.db('events_tycdates').select(1).where({
      tycdate_id: tycdateID,
      event_id: eventID
    }).then((rows) => {
      return !!rows.length
    })
  }

  registerUser (tycdateID, userID) {
    return this.db('tycdates_users').insert({
      tycdate_id: tycdateID,
      user_id: userID
    })
  }

  getUsers (id, returning) {
    return this.getRelationVia(id, 'users', {
      table: 'tycdates_users',
      sourceCol: 'tycdate_id',
      goalCol: 'user_id'
    }, returning)
  }

  async atomicDelete (tycdateID) {
    await this.db('events_tycdates').where('tycdate_id', tycdateID).del()
    await this.db('tycdates_users').where('tycdate_id', tycdateID).del()
    await this.table.where('id', tycdateID).del()
  }

  // -- Overloaded Functions -- //
  // ---------- None ---------- //
}

export default Tycdate
