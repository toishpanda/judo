import BasicTable from './parents/BasicTable'

class Club extends BasicTable {
  constructor (db) {
    super('clubs', db)
  }

  // -- New Functions -- //

  getUsers (id, returning) {
    return this.getRelationVia(id, 'users', {
      table: 'clubs_users',
      sourceCol: 'club_id',
      goalCol: 'user_id'
    }, returning)
  }

  attachUser (clubID, userID) {
    return this.db('users').where('id', userID).update({ active: true })
      .then(() => {
        return this.db('clubs_users').insert({
          club_id: clubID,
          user_id: userID
        })
      })
  }

  attachEvent (clubID, eventID) {
    return this.db('clubs_events').insert({
      club_id: clubID,
      event_id: eventID
    })
  }

  getClubsEvents (id, returning) {
    return this.getRelationVia(id, 'events', {
      table: 'clubs_events',
      sourceCol: 'club_id',
      goalCol: 'event_id'
    }, returning)
  }

  async createJoinRequest (clubID, userID) {
    let invalid = await this.userHasPendingJoinRequest(clubID, userID)
    invalid = invalid || await this.userJoinedClub(clubID, userID)
    if (invalid) return Promise.reject(new Error('You cannot join a club twice'))
    return this.db('club_join_requests').insert({
      club_id: clubID,
      user_id: userID
    })
  }

  getJoinRequests (id, returning) {
    return this.getRelationVia(id, 'users', {
      table: 'club_join_requests',
      sourceCol: 'club_id',
      goalCol: 'user_id'
    }, returning)
  }

  countJoinRequests (id) {
    return this.db('club_join_requests').where({
      club_id: id
    }).count().then((res) => res[0].count)
  }

  deleteJoinRequest (clubID, userID) {
    return this.db('club_join_requests').where({
      club_id: clubID,
      user_id: userID
    }).del()
  }

  userHasPendingJoinRequest (clubID, userID) {
    return this.db('club_join_requests').where({
      club_id: clubID,
      user_id: userID
    }).then((res) => res.length > 0)
  }

  userJoinedClub (clubID, userID) {
    return this.db('clubs_users').where({
      club_id: clubID,
      user_id: userID
    }).then((res) => res.length > 0)
  }

  // -- Overloaded Functions -- //
  // ---------- None ---------- //
}

export default Club
