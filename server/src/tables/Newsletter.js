import BasicTable from './parents/BasicTable'

class Newsletter extends BasicTable {
  constructor (db) {
    super('newsletters', db)
  }

  // -- New Functions -- //
  // ------ None ------- //

  // -- Overloaded Functions -- //
  // ---------- None ---------- //
}

export default Newsletter
