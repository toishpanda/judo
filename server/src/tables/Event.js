import _omit from 'lodash.omit'
import BasicTable from './parents/BasicTable'

class Event extends BasicTable {
  constructor (db) {
    super('events', db)
  }

  // -- New Functions -- //

  getTycdates (id, returning) {
    return this.getRelationVia(id, 'tycdates', {
      table: 'events_tycdates',
      sourceCol: 'event_id',
      goalCol: 'tycdate_id'
    }, returning)
  }

  attachTycdate (eventID, tycdateID) {
    return this.db('events_tycdates').insert({
      event_id: eventID,
      tycdate_id: tycdateID
    })
  }

  /** Be careful. This deletes EVERYTHING associated with this event except users + clubs. */
  async atomicDelete (eventID) {
    const tycdateIDs = await this.getTycdates(eventID, ['id']).then((tycdates) => tycdates.map((tycdate) => tycdate.id))
    if (tycdateIDs) {
      // Delete Event->Tycdate Relation
      await this.db('events_tycdates').where('event_id', eventID).del()

      // Delete Tycdate->User Relation
      Promise.all(tycdateIDs.map(async (tycdateID) => {
        await this.db('tycdates_users').where('tycdate_id', tycdateID).del()
      }))

      // Delete Tycdate
      await this.db('tycdates').whereIn('id', tycdateIDs).del()
    }

    // Delete Event->Club Relation
    await this.db('clubs_events').where('event_id', eventID).del()

    await this.find(eventID).del()
  }

  getClubs (id, returning) {
    return this.getRelationVia(id, 'clubs', {
      table: 'clubs_events',
      sourceCol: 'event_id',
      goalCol: 'club_id'
    }, returning)
  }

  attachClub (eventID, clubID) {
    return this.db('clubs_events').insert({
      club_id: clubID,
      event_id: eventID
    })
  }

  // -- Overloaded Functions -- //

  async copy (id) {
    const newItem = await super.copy(id)
    const srcTycdateIds = await this.db('events_tycdates').where('event_id', id).map((row) => {
      return row.tycdate_id
    })

    const srcTycdates = await this.db('tycdates').whereIn('id', srcTycdateIds)
    await Promise.all(srcTycdates.map(async (row) => {
      const newTycdate = await this.db('tycdates').insert(_omit(row, ['id'])).returning(['id'])
      await this.attachTycdate(newItem.id, newTycdate[0].id)
    }))

    const srcClubRelations = await this.db('clubs_events').where('event_id', id).map((row) => {
      row.event_id = newItem.id
      return row
    })
    await this.db('clubs_events').insert(srcClubRelations)

    return newItem
  }
}

export default Event
