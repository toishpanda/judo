import User from './User'
import Club from './Club'
import Newsletter from './Newsletter'
import Event from './Event'
import Tycdate from './Tycdate'
import Message from './Message'
import Invoice from './Invoice'
import Tycdategroup from './Tycdategroup'

/**
 * Generate the tables list that get's injected into all controllers
 * @param {Object} db - The Knex DB API
*/
function generateTables (db) {
  const tables = {
    User: new User(db),
    Club: new Club(db),
    Newsletter: new Newsletter(db),
    Event: new Event(db),
    Tycdate: new Tycdate(db),
    Message: new Message(db),
    Invoice: new Invoice(db),
    Tycdategroup: new Tycdategroup(db)
  }

  return tables
}

export default generateTables
