import BasicTable from './parents/BasicTable'

import _omit from 'lodash.omit'

class User extends BasicTable {
  constructor (db) {
    super('users', db)
  }

  // -- New Functions -- //

  async setBelt (id, newBelt) {
    const from = (await this.find(id)).belt

    await this.update(id, {
      belt: newBelt
    })

    this.db('logevents').insert({
      type: 'belt-change',
      data: {
        user_id: id,
        from,
        to: newBelt
      }
    })
  }

  // CLUBS AND EVENTS

  getClubs (id, returning) {
    return this.getRelationVia(id, 'clubs', {
      table: 'clubs_users',
      sourceCol: 'user_id',
      goalCol: 'club_id'
    }, returning)
  }

  async getRegisteredEvents (id, returning) {
    // Get all of the user's registered TYCDATEs
    let tycdates = await this.getRelationVia(id, 'tycdates', {
      table: 'tycdates_users',
      sourceCol: 'user_id',
      goalCol: 'tycdate_id'
    }, ['*'])

    // Then get all of those TYCDATEs respective events
    let events = await Promise.all(tycdates.map((tycdate) => {
      return this.getRelationVia(tycdate.id, 'events', {
        table: 'events_tycdates',
        sourceCol: 'tycdate_id',
        goalCol: 'event_id'
      }, ['*']).then((res) => res[0])
    }))

    // Merge the two lists, removing duplicates
    return tycdates.reduce((acc, tycdate, index) => {
      if (acc.some((eventGroup) => eventGroup.event.id === events[index].id)) {
        let eventIndex = acc.findIndex((eventGroup) => eventGroup.event.id === events[index].id)
        acc[eventIndex].tycdates.push(tycdate)
      } else {
        acc.push({
          event: events[index],
          tycdates: [tycdate]
        })
      }

      return acc
    }, [])
  }

  attachClub (userID, clubID) {
    return this.update(userID, { active: true })
      .then(() => {
        return this.db('clubs_users').insert({
          club_id: clubID,
          user_id: userID
        })
      })
  }

  isUsersDojoshu (dojoshuID, userID) {
    return this.getClubs(userID).then((clubs) => {
      const club = clubs[0] // Get primary club
      return club ? club.dojoshu_id === dojoshuID : false
    })
  }

  /**
   * Refresh the logged in session with data from DB
   * @param {object} ctx - Koa request context
   */
  async refreshUserCache (ctx) {
    ctx.session.loggedInAs = await this.find(ctx.session.loggedInAs.id)
  }

  // -- Overloaded Functions -- //

  /* Overwriting some default functions to make sure that password hash is NEVER accidentally leaked to frontend */
  async find (id) {
    const user = await super.find(id)
    return _omit(user, ['password'])
  }
  async findBy (key, value) {
    const user = await super.findBy(key, value)
    return _omit(user, ['password'])
  }
  findByUnsafe (key, value) {
    return super.findBy(key, value)
  }
  /*******/
}

export default User
