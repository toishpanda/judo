import _omit from 'lodash.omit'

class BasicTable {
  constructor (table, db) {
    this.db = db
    this.tableName = table
  }

  get table () { return this.db(this.tableName) }

  // These timestamp functions only work on Knex Queries! //
  updateTimestamp (query) {
    return query.update('updated_at', this.db.fn.now())
  }

  createTimestamp (query) {
    return this.updateTimestamp(
      query.update('created_at', this.db.fn.now())
    )
  }
  // --------------------------------------------------- //

  select (returning) {
    return this.table.select(returning)
  }

  find (id, returning) {
    if (returning) return this.table.where('id', '=', id).select(returning).first()
    else return this.table.where('id', '=', id).first()
  }

  exists (id) {
    return this.table.column(['id']).select(1).where('id', '=', id).first().then((item) => item !== undefined)
  }

  findBy (key, value) {
    return this.table.where(key, '=', value).first()
  }

  unique (key, value) {
    return this.table.where(key, '=', value).select('id').first()
      .then((res) => {
        // res can be an object, or undefined, hence this stupid shit
        return res === undefined
      })
  }

  insert (data, returning) {
    data.created_at = this.db.fn.now()
    data.updated_at = this.db.fn.now()

    if (returning) {
      return this.table.insert(data).returning(returning).then((res) => res[0])
    } else {
      return this.table.insert(data)
    }
  }

  update (id, data, returning) {
    data.updated_at = this.db.fn.now()

    if (returning) {
      return this.table.where('id', '=', id).update(data).returning(returning).then((res) => res[0])
    } else {
      return this.table.where('id', '=', id).update(data)
    }
  }

  async updateWhere (key, value, data, returning) {
    data.updated_at = (new Date())

    if (returning) {
      await this.table.where(key, '=', value).update(data).returning(returning).then((res) => res[0])
    } else {
      await this.table.where(key, '=', value).update(data)
    }
  }

  async copy (id) {
    const item = _omit(await this.find(id), ['username', 'id'])
    const newItem = Object.keys(item).reduce((acc, key) => {
      if (key === 'tags') return Object.assign(acc, { [key]: JSON.stringify(item[key]) })
      return Object.assign(acc, { [key]: item[key] })
    }, {})
    return this.insert(newItem, ['*'])
  }

  getColumns () {
    return this.table.columnInfo().then((info) => {
      return Object.keys(info)
    })
  }

  // Relations //

  /**
   * Gets a referenced value from another table, using a pivot table. If you are getting data from the users table, the password is stripped from the result. If you need the user's password hash, use this function to get the ID, then use `tables.User.findUnsafe(id)`
   * @prop {number} sourceID - Source table's ID, refers to value in Pivot's sourceCol
   * @prop {number} goalTable - Refers to table of data we want via the Pivot table
   * @prop {object} pivot - Object with data about the pivot table
   * @prop {string} pivot.table - Name of the pivot table
   * @prop {string} pivot.sourceCol - Name of the source id column inside the pivot table (should have IDs of rows in the table you're COMING FROM)
   * @prop {string} pivot.goalCol - Name of the goal id column inside the pivot table (should have IDs of rows in the table you're TRYING TO GET DATA FROM)
   */
  getRelationVia (sourceID, goalTable, pivot, returning) {
    return this.db(pivot.table)
      .join(goalTable, pivot.table + '.' + pivot.goalCol, '=', goalTable + '.id')
      .select(returning ? returning.map((col) => goalTable + '.' + col) : goalTable + '.*')
      .where(pivot.sourceCol, '=', sourceID)
      .orderBy(goalTable + '.id', 'desc')
      .then((results) => {
        if (goalTable === 'users') {
          return results.map((result) => {
            // Removing password hash from results (Just in case!), if you need the user's hash, use "findUnsafe" instead
            if (Object.keys(result).indexOf('password')) return _omit(result, 'password')
            return result
          })
        } else return results
      })
  }
}

export default BasicTable
