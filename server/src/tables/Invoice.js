import BasicTable from './parents/BasicTable'

class Invoice extends BasicTable {
  constructor (db) {
    super('invoices', db)

    this.CONSTS = Object.freeze({
      SPECIALFEE: Symbol('Special Fee'),
      REGISTRATIONFEE: Symbol('Registration Fee'),
      CLUBFEE: Symbol('Club Fee')
    })
  }

  // -- New Functions -- //

  getInvoicesFor (table, entityID) {
    return this.db(`invoices`).where({
      to_id: entityID,
      to_type: table
    })
  }

  /**
   * Attach a line item id to an invoice
   * @param {Number} invoiceID - Invoice to attach to
   * @param {Number} lineitemIDs - Line item id to attach
   */
  attachItem (invoiceID, lineitemID) {
    return this.db('invoices_lineitems').insert({
      invoice_id: invoiceID,
      lineitem_id: lineitemID
    })
  }

  /**
   * Attach a list of line item ids to an invoice
   * @param {Number} invoiceID - Invoice to attach to
   * @param {Array} lineitemIDs - Array of line item ids to attach
   */
  attachItems (invoiceID, lineitemIDs) {
    return Promise.all(lineitemIDs.map((lineitemID) => this.attachItem(invoiceID, lineitemID)))
  }

  /**
   * Create a new line item to be used in a invoice
   * @param {*} type - The type of line item, this takes a symbol from "tables.Invoice.CONSTS[...]"
   * @param {*} name - The name of the line item
   * @param {*} description  - A description of the line item
   * @param {*} value  - Price of the line item in dollars, rounded to 2 decimal places
   */
  newLineItem (type, name, description, value) {
    const lineitem = {
      name,
      description,
      value,
      is_registration_fee: (type === this.CONSTS.REGISTRATIONFEE),
      is_club_fee: (type === this.CONSTS.CLUBFEE),
      is_special_fee: (type === this.CONSTS.SPECIALFEE)
    }
    return this.db('lineitems').insert(lineitem, ['id']).then((lineitem) => lineitem[0].id)
  }

  // -- Overloaded Functions -- //
  // ---------- None ---------- //
}

export default Invoice
