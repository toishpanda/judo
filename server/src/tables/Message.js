import BasicTable from './parents/BasicTable'

class Message extends BasicTable {
  constructor (db) {
    super('messages', db)
  }

  // -- New Functions -- //

  attachUser (messageID, userID) {
    return this.db('messages_users').insert({
      message_id: messageID,
      user_id: userID,
      unread: true
    })
  }

  attachUsers (messageID, userIDs) {
    return Promise.all(userIDs.map((userID) => {
      return this.attachUser(messageID, userID)
    }))
  }

  markAsRead (messageID, userID) {
    return this.db('messages_users').where({
      message_id: messageID,
      user_id: userID
    }).update({
      unread: false
    })
  }

  isUnread (messageID, userID) {
    return this.db('messages_users').where({
      message_id: messageID,
      user_id: userID
    }).first().then((messageUser) => {
      return messageUser.unread
    })
  }

  isRead (messageID, userID) {
    return this.isUnread(messageID, userID).then((read) => !read)
  }

  isSentTo (messageID, userID) {
    return this.db('messages_users').where({
      message_id: messageID,
      user_id: userID
    }).count().then((res) => {
      return res[0].count > 0
    })
  }

  // USERS

  /**
   * Get a list of messages on the user's account, optionally filtered
   * @param {Number} id - User's ID
   * @param {('all'|'unread'|'read')} mode - Filtration to apply to message list
   */
  getUsersMessages (id, mode, returning) {
    return this.db('messages_users')
      .join('messages', 'messages_users.message_id', '=', 'messages.id')
      .where(mode === 'all' ? {
        'user_id': id
      } : {
        'user_id': id,
        'unread': mode === 'unread'
      })
      .select(returning || ['*'])
  }

  /**
   * Get a count of messages on the user's account, optionally filtered
   * @param {Number} id - User's ID
   * @param {('all'|'unread'|'read')} mode - Filtration to apply to message list
   */
  countUsersMessages (id, mode) {
    return this.db('messages_users')
      .where(mode === 'all' ? {
        'user_id': id
      } : {
        'user_id': id,
        'unread': mode === 'unread'
      })
      .count()
      .then((res) => parseInt(res[0].count))
  }

  /**
   * Delete a message from a user's inbox
   * @param {Number} messageID - The message's ID
   * @param {Number} userID - The user's ID
   */
  deleteFromUsersInbox (messageID, userID) {
    return this.db('messages_users').where({
      'user_id': userID,
      'message_id': messageID
    }).del()
  }

  /** Be careful. This deletes EVERYTHING associated with this message. */
  async atomicDelete (messageID) {
    // Delete Message->User Relation
    await this.db('messages_users').where('message_id', messageID).del()
    await this.find(messageID).del()
  }

  // -- Overloaded Functions -- //
  // ---------- None ---------- //
}

export default Message
