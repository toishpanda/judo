const MONTHS = [
  {
    key: 'JAN',
    name: 'January'
  }, {
    key: 'FEB',
    name: 'February'
  }, {
    key: 'MAR',
    name: 'March'
  }, {
    key: 'APR',
    name: 'April'
  }, {
    key: 'MAY',
    name: 'May'
  }, {
    key: 'JUN',
    name: 'June'
  }, {
    key: 'JUL',
    name: 'July'
  }, {
    key: 'AUG',
    name: 'August'
  }, {
    key: 'SEP',
    name: 'September'
  }, {
    key: 'OCT',
    name: 'October'
  }, {
    key: 'NOV',
    name: 'November'
  }, {
    key: 'DEC',
    name: 'December'
  }
]

const MAX_SEARCHABLE_YEARS = 50

const CURRENT_YEAR = (new Date()).getFullYear()

export { MONTHS, MAX_SEARCHABLE_YEARS, CURRENT_YEAR }
