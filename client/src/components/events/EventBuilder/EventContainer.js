import { APIRequest } from '@/apiHelper.js'

import _omit from 'lodash.omit'
import blobUtil from 'blob-util'

export class EventContainer {
  constructor (id) {
    this.event = this.createEventProxy({
      id: (id !== undefined ? id : null),
      draft: true,
      name: '',
      avatar: null,
      poster: null,
      tags: [],
      discount_rule: 4,
      discount_effect: 1,
      register_start: new Date().toISOString(),
      register_end: new Date().toISOString()
    })
    this.tycdates = []
    this.mutated = false
  }

  createEventProxy (event) {
    return new Proxy(event, this.createMutationHandler())
  }

  createMutationHandler () {
    const container = this
    return {
      set (target, key, value) {
        container.mutated = true
        target[key] = value
        return true
      }
    }
  }

  omitFiles (event) {
    return _omit(event, ['avatar', 'poster'])
  }

  containsFiles (event) {
    return ((event.avatar || '').slice(0, 5) === 'data:') || ((event.poster || '').slice(0, 5) === 'data:')
  }

  async B64ToFormData (b64) {
    const data = new FormData()
    const blob = await blobUtil.dataURLToBlob(b64)
    data.append('avatar', blob)
    return data
  }

  async commit () {
    if (this.event.id !== null) {
      await APIRequest('PUT', `events/${this.event.id}`, this.omitFiles(this.event))
      if ((this.event.avatar || '').slice(0, 5) === 'data:') {
        const data = await this.B64ToFormData(this.event.avatar)
        await APIRequest('PUT', `events/${this.event.id}/avatar`, data)
      }
      if ((this.event.poster || '').slice(0, 5) === 'data:') {
        const data = await this.B64ToFormData(this.event.poster)
        await APIRequest('PUT', `events/${this.event.id}/poster`, data)
      }
      this.mutated = false
      return this.event
    } else {
      const res = await APIRequest('POST', `events`, this.omitFiles(this.event))
      if (res.success) {
        if (this.containsFiles(this.event)) {
          if ((this.event.avatar || '').slice(0, 5) === 'data:') {
            const data = await this.B64ToFormData(this.event.avatar)
            await APIRequest('PUT', `events/${res.id}/avatar`, data)
          }
          if ((this.event.poster || '').slice(0, 5) === 'data:') {
            const data = await this.B64ToFormData(this.event.poster)
            await APIRequest('PUT', `events/${res.id}/poster`, data)
          }
        }
        this.event.id = res.id
        await this.pull()
        this.mutated = false
        return this.event
      } else {
        throw Error('Check your data, something is incorrect')
      }
    }
  }

  async pull () {
    if (this.event.id !== null) {
      const eventContainer = await APIRequest('GET', `events/${this.event.id}`)
      this.event = this.createEventProxy(eventContainer.event)
      this.tycdates = eventContainer.tycdates
      this.mutated = false
      return this.event
    }
    return false
  }

  async createNew () {
    const res = await APIRequest('POST', `events`, this.event)
    return res
  }
}
