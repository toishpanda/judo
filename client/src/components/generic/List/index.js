import List from './List'

import ListDeleteBtn from './ListDeleteBtn'
import ListApproveBtn from './ListApproveBtn'

export default {
  install (Vue) {
    Vue.component('List', List)
    Vue.component('ListDeleteBtn', ListDeleteBtn)
    Vue.component('ListApproveBtn', ListApproveBtn)
  }
}
