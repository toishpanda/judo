import Modal from './Modal'
import HTML from 'vue-html'

export default {
  install (Vue) {
    Vue.use(HTML)
    Vue.component('Modal', Modal)

    const ModalComponent = Vue.extend(Object.assign(Modal, { }))

    /**
     * vm.$modal.new(...)
     * Create a new Modal without having to explicitly declare a Modal instance
     * @prop {object} props - Object containing any props that you'd normall pass into Modal btns, title, etc)
     * @prop {string} content - A string containing the HTML content for the Modal. It'll be wrapped in a <div> before being displayed.
     */
    Vue.prototype.$modal = {
      new: function (props, content) {
        const ModalInstance = new ModalComponent({ propsData: Object.assign(props, {functional: true}) })
        if (content) {
          ModalInstance.$slots.default = [ModalInstance.$html(`<div content>${content}</div>`)]
        }
        const mountEl = document.createElement('div')
        document.body.appendChild(mountEl)
        ModalInstance.$mount(mountEl)
        return ModalInstance
      }
    }
  }
}
