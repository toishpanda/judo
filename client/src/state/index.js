import Vue from 'vue'
import Vuex from 'vuex'

import mutations from './mutations'
import actions from './actions'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    user: {
      _refreshed: null,
      loggedIn: false,
      loggedInAs: null,
      new: false,
      verified: false
    },
    counts: {
      _refreshed: null,
      messages: {
        unread: 0
      },
      dojoshuclub: {
        paid: 0,
        unpaid: 0,
        requests: 0
      }
    },
    online: (window.navigator.onLine === undefined ? true : window.navigator.onLine),
    error: null,
    notice: null,
    cache: { users: {} }
  },
  mutations,
  actions
})

export default store
