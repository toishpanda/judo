function SHOW_ERROR (store, payload) {
  store.error = payload.message
  return store
}

function CLEAR_ERROR (store, payload) {
  store.error = null
  return store
}

function SHOW_NOTICE (store, payload) {
  store.notice = payload.message
  return store
}

function CLEAR_NOTICE (store, payload) {
  store.notice = null
  return store
}

function GO_OFFLINE (store, payload) {
  store.online = false
  store.error = 'You are currently offline. To resume using the site, please connect to the internet.'
  return store
}

function GO_ONLINE (store, payload) {
  store.online = true
  store.error = null
  return store
}

export { SHOW_ERROR, CLEAR_ERROR, SHOW_NOTICE, CLEAR_NOTICE, GO_OFFLINE, GO_ONLINE }
