import * as user from './user.js'
import * as error from './error.js'
import * as counts from './counts.js'
import * as cache from './cache.js'

export default Object.assign({}, user, error, counts, cache)
