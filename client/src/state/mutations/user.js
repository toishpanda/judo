function LOGIN (store, payload) {
  store.user.loggedIn = true
  store.user.loggedInAs = payload.user
  return store
}

function LOGOUT (store) {
  store.user.loggedIn = false
  store.user.loggedInAs = null
  return store
}

function SIGNUP (store) {
  store.user.new = true
  return store
}

function VERIFY_STATE (store, payload) {
  store.user.verified = true
  return store
}

function UPDATE_USER_REFRESH_TIME (store) {
  store.user._refreshed = new Date()
  return store
}

export { LOGIN, LOGOUT, SIGNUP, VERIFY_STATE, UPDATE_USER_REFRESH_TIME }
