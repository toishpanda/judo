function UPDATE_UNREAD_MESSAGE_COUNT (store, payload) {
  store.counts.messages.unread = payload
  return store
}

function UPDATE_UNPAID_USER_COUNT (store, payload) {
  store.counts.dojoshuclub.unpaid = payload
  return store
}

function UPDATE_PAID_USER_COUNT (store, payload) {
  store.counts.dojoshuclub.paid = payload
  return store
}

function UPDATE_REQUEST_COUNT (store, payload) {
  store.counts.dojoshuclub.requests = payload
  return store
}

function UPDATE_COUNTS_REFRESH_TIME (store) {
  store.counts._refreshed = new Date()
  return store
}

export { UPDATE_UNREAD_MESSAGE_COUNT, UPDATE_UNPAID_USER_COUNT, UPDATE_PAID_USER_COUNT, UPDATE_REQUEST_COUNT, UPDATE_COUNTS_REFRESH_TIME }
