import { addMinutes } from 'date-fns'

/**
 * Store some data in the session cache, useful for reducing network calls
 * @param {*} store - Vuex Store
 * @param {*} payload - Data for cache item
 * @param {*} payload.group - The cache group name (e.g., users)
 * @param {*} payload.key - The cached data's key (e.g., a username)
 * @param {*} payload.data - The data to store (e.g., user object)
 */
function CACHE_DATA (store, payload) {
  store.cache[payload.group][payload.key] = {
    invalidAfter: addMinutes(new Date(), 5),
    data: payload.data
  }
  return store
}

export { CACHE_DATA }
