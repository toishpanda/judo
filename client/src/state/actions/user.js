import { APIRequest } from '@/apiHelper.js'

async function login (store, payload) {
  await APIRequest('post', 'auth/login', {
    username: payload.username,
    password: payload.password
  }).then((res) => {
    if (res.success) {
      if (store.state.error) {
        store.commit({
          type: 'CLEAR_ERROR'
        })
      }

      store.commit({
        type: 'LOGIN',
        user: res.user
      })
      store.commit('VERIFY_STATE')
    } else {
      store.commit({
        type: 'SHOW_ERROR',
        message: 'The username or password you entered was incorrect.'
      })
    }
  })
}

async function logout (store) {
  await APIRequest('get', 'auth/logout')

  store.commit({
    type: 'LOGOUT'
  })
}

async function signup (store, payload) {
  await APIRequest('post', 'users', {
    firstname: payload.firstname,
    lastname: payload.lastname,
    username: payload.username,
    password: payload.password,
    email: payload.email
  }).then(async (res) => {
    if (res.success) {
      if (store.state.error) {
        store.commit({
          type: 'CLEAR_ERROR'
        })
      }

      store.commit({
        type: 'SIGNUP'
      })

      await store.dispatch('login', payload)
    } else {
      store.commit({
        type: 'SHOW_ERROR',
        message: res.message
      })
    }
  })
}

async function hydrateLogin (store) {
  const res = (await APIRequest('get', 'auth/check'))

  if (res.loggedIn) {
    store.commit({
      type: 'LOGIN',
      user: res.user
    })
    store.commit('VERIFY_STATE')
    store.commit('UPDATE_USER_REFRESH_TIME')
  }
}

export { login, logout, signup, hydrateLogin }
