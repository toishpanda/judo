import * as user from './user.js'
import * as counts from './counts.js'

export default Object.assign({}, user, counts)
