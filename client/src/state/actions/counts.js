import { APIRequest } from '@/apiHelper.js'

async function refreshNotifications (store) {
  if (!store.state.user.loggedIn) return null
  store.commit('UPDATE_COUNTS_REFRESH_TIME')
  await APIRequest('GET', 'messages/filter/unread/count')
    .then((res) => {
      if (res.success) {
        if (store.state.counts.messages.unread !== res.total) store.commit('UPDATE_UNREAD_MESSAGE_COUNT', res.total)
      } else {
        store.commit('SHOW_ERROR', res)
      }
    })

  const userIsDojoshu = store.state.user.loggedInAs.clubs.some((club) => {
    return club.dojoshu.username === store.state.user.loggedInAs.username
  })

  if (userIsDojoshu) {
    const clubID = store.state.user.loggedInAs.clubs.find((club) => {
      return club.dojoshu.username === store.state.user.loggedInAs.username
    }).id

    await APIRequest('GET', `clubs/${clubID}/users/unpaid/count`)
      .then((res) => {
        if (res.success) {
          if (store.state.counts.dojoshuclub.unpaid !== res.total) store.commit('UPDATE_UNPAID_USER_COUNT', res.total)
        } else {
          store.commit('SHOW_ERROR', res)
        }
      })
      .then(() => {
        return APIRequest('GET', `clubs/${clubID}/users/paid/count`)
          .then((res) => {
            if (res.success) {
              if (store.state.counts.dojoshuclub.paid !== res.total) store.commit('UPDATE_PAID_USER_COUNT', res.total)
            } else {
              store.commit('SHOW_ERROR', res)
            }
          })
      })
      .then(() => {
        return APIRequest('GET', `clubs/${clubID}/requests/count`)
          .then((res) => {
            if (res.success) {
              if (store.state.counts.dojoshuclub.requests !== res.total) store.commit('UPDATE_REQUEST_COUNT', res.total)
            } else {
              store.commit('SHOW_ERROR', res)
            }
          })
      })
  }
}

export { refreshNotifications }
