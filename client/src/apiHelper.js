import axios from 'axios'

import { isBefore } from 'date-fns'

const apiRoot = (process.env.NODE_ENV === 'development' ? 'http://localhost:8080/api/' : '/api/')
const staticRoot = (process.env.NODE_ENV === 'development' ? 'http://localhost:8080/static/' : '/static/')

function makeRequest (options) {
  return axios(options).then((res) => res.data).catch((res) => {
    return {
      success: res.response.data.success,
      message: res.response.data.message,
      response: res
    }
  })
}

/**
 * Makes a request to the specified endpoint via Axios. Returns a promise.
 * @param {String} method - REST request method in lowercase. I.E., 'get', 'post', 'update'.
 * @param {String} endpoint - REST endpoint with out leading slash.
 * @param {String} [data] - Data object that is to be sent with requests that support it.
 * @param {object} [options] - Options passed into Axios. See the Axios documentation to learn more.
 */
export function APIRequest (method, endpoint, data, options, store) {
  const defaultOptions = {
    method,
    withCredentials: true,
    url: apiRoot + endpoint,
    data: data || null
  }

  const isUserEndpoint = /^users\/[a-zA-Z0-9-]*$/
  if (store && method.toLowerCase() === 'get' && isUserEndpoint.test(endpoint)) {
    const username = endpoint.split('/')[1]
    const cacheItem = store.state.cache.users[username]
    if (Object.keys(store.state.cache.users).includes(username) && isBefore(new Date(), cacheItem.invalidAfter)) {
      return new Promise((resolve, reject) => resolve({
        success: true,
        user: cacheItem.data
      }))
    } else {
      return makeRequest(Object.assign(defaultOptions, options || {})).then((res) => {
        if (res.success) store.commit('CACHE_DATA', { group: 'users', key: username, data: res.user })
        return res
      })
    }
  }

  return makeRequest(Object.assign(defaultOptions, options || {}))
}

/**
 * Request + cache a user object, use instead of APIRequest so we can cache user objects
 * @param {*} username - Username of user to request
 * @param {*} store - Vuex store, normally `this.$store`
 */
export function GetUser (username, store) {
  return APIRequest('GET', `users/${username}`, null, {}, store)
}

/**
 * Returns URL to the static file
 * @param {String} file - File name
 */
export function StaticFile (file) {
  if ((file || '').slice(0, 5) === 'data:') return file
  return file === null ? '' : `${staticRoot}${file}`
}

/**
 * Gets a static file and returns a Base64 copy of it. Useful for images.
 * @param {String} file - File name
 */
export function FetchStaticFile (file) {
  const url = StaticFile(file)
  return axios.get(url, { responseType: 'arraybuffer' })
    .then(response => new Buffer(response.data, 'binary').toString('base64'))
}
