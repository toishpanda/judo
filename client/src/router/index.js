import Vue from 'vue'
import Router from 'vue-router'

import Root from './root'
import Login from './login'
import Dashboard from './dashboard/'
import Dojoshu from './dojoshu/'
import Profile from './profile'

Vue.use(Router)

const routers = [ Root, Login, Dashboard, Dojoshu, Profile ]

function generateRouter (store) {
  const router = new Router(
    routers.map((router) => router())
           .reduce((acc, instance) => {
             acc.routes = acc.routes.concat(instance)
             return acc
           }, { routes: [] })
    )

  router.beforeEach(async (to, from, next) => {
    if (to.matched.some(record => record.meta.authRequired)) {
      if (!store.state.user.verified) {
        await store.dispatch('hydrateLogin')
      }
      if (!store.state.user.loggedIn) {
        next({
          path: '/login',
          query: { redirect: to.fullPath }
        })
      } else {
        next()
      }
    } else {
      next()
    }
  })

  return router
}

export default generateRouter
