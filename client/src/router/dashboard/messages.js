import Inbox from '@/pages/Messages/Inbox'
import NewMessage from '@/pages/Messages/NewMessage'
import Message from '@/pages/Messages/Message'

function MessagesRouter () {
  return [
    {
      path: '/dashboard/messages',
      name: 'Inbox',
      component: Inbox,
      meta: { authRequired: true }
    },
    {
      path: '/dashboard/messages/new',
      name: 'New Message',
      component: NewMessage,
      meta: { authRequired: true }
    },
    {
      path: '/dashboard/messages/:messageID',
      name: 'Message',
      component: Message,
      meta: { authRequired: true },
      props: true
    }
  ]
}

export default MessagesRouter
