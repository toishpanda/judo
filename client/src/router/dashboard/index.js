import Dashboard from '@/pages/Dashboard/Dashboard'
import RegisteredEvents from '@/pages/Dashboard/Events'

import MessagesRouter from './messages'

function DashboardRouter () {
  return [
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      meta: { authRequired: true }
    },
    {
      path: '/dashboard/events',
      name: 'Your Registered Events',
      component: RegisteredEvents,
      meta: { authRequired: true }
    }
  ].concat(MessagesRouter())
}

export default DashboardRouter
