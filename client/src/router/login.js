import Login from '@/pages/Login'

function LoginRouter () {
  return [
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
}

export default LoginRouter
