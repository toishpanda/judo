import UserProfile from '@/pages/Profile/User'
import ClubProfile from '@/pages/Profile/Club'

function ProfileRouter () {
  return [
    {
      path: '/profile/:username',
      name: 'Member Profile',
      component: UserProfile,
      props: true
    },
    {
      path: '/club/:id',
      name: 'Club Profile',
      component: ClubProfile,
      props: true
    }
  ]
}

export default ProfileRouter
