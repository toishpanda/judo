function RootRouter () {
  return [
    {
      path: '/',
      redirect: '/dashboard'
    }
  ]
}

export default RootRouter
