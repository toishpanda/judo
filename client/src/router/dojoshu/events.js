import ClubsEvents from '@/pages/Events/ClubsEvents'
import NewEvent from '@/pages/Events/NewEvent'
import EditEvent from '@/pages/Events/EditEvent'

function EventsRouter () {
  return [
    {
      path: '/dojoshu/events',
      name: 'Club Events List',
      component: ClubsEvents,
      meta: { authRequired: true }
    }, {
      path: '/dojoshu/events/new',
      name: 'New Event',
      component: NewEvent,
      meta: { authRequired: true }
    }, {
      path: '/dojoshu/events/:id',
      name: 'Edit Event',
      component: EditEvent,
      meta: { authRequired: true }
    }
  ]
}

export default EventsRouter
