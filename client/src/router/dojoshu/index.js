import ClubMembers from '@/pages/Dojoshu/ClubMembers'
import ClubRequests from '@/pages/Dojoshu/ClubRequests'

import EventsRouter from './events'

function DojoshuRouter () {
  return [
    {
      path: '/dojoshu/members',
      name: 'Club Members',
      component: ClubMembers,
      meta: { authRequired: true }
    },
    {
      path: '/dojoshu/requests',
      name: 'Join Requests',
      component: ClubRequests,
      meta: { authRequired: true }
    }
  ].concat(EventsRouter())
}

export default DojoshuRouter
