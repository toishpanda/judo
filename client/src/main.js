// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import { sync } from 'vuex-router-sync'

import routerGenerator from './router'
import store from './state'

import Page from './pages/Page'

import Modal from './components/generic/Modal'
Vue.use(Modal)

Vue.component('page', Page)

Vue.config.productionTip = false

const router = routerGenerator(store)
sync(store, router)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  data: { env: process.env },
  template: '<App/>',
  components: { App },
  async beforeCreate () {
    await this.$store.dispatch('hydrateLogin')
    await this.$store.dispatch('refreshNotifications')
  },
  mounted () {
    this.$watch('$store.state.route.path', () => {
      if (this.$store.state.error !== null) this.$store.commit('CLEAR_ERROR')
      if (this.$store.state.notice !== null) this.$store.commit('CLEAR_NOTICE')
    })

    window.addEventListener('online', () => this.$store.commit('GO_ONLINE'))
    window.addEventListener('offline', () => this.$store.commit('GO_OFFLINE'))
  }
})
