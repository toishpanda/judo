Hey!
So, I'm working on automating this process more. But there are still some things to set up after starting the Docker Setup:

1. Create the databases at http://localhost:8083
   - pyro
   - judo
2. Run this inside the nginx container:
   `php artisan install`
   to set up Pyro
3. Inside /static, on your HOST system (it's inside a Docker Volume, so any changes made there are mirrored in the container) create a copy of `.env.example` and name it `.env`
4. If you get an error about the Reflection class on the static site, run this inside the nginx container:
    cd /var/www/static; composer dump-autoload